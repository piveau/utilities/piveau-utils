package io.piveau.dcatap.classes;

import io.piveau.dcatap.properties.BaseProperty;
import io.piveau.utils.PropertyHelper;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import java.util.ArrayList;
import java.util.List;

public class BaseClass {

    public String uri;
    protected Model model;
    protected Resource resource;
    private Resource classNamespace;

    private List<String> mandatoryProperties;
    private List<BaseProperty> attachedProperties;

    public BaseClass(String uri, Resource classNamespace) {
        this.uri = uri;
        this.classNamespace = classNamespace;
        this.mandatoryProperties = new ArrayList();
        this.attachedProperties = new ArrayList();
    }

    public void validateSchema() {
        List mandatories = PropertyHelper.mandatoryClassProperties.get(this.getClass().getSimpleName());
        if (mandatories != null) {
            mandatories.forEach(property -> {
                if (!mandatoryProperties.contains(property)) {
                    throw new NullPointerException("Mandatory Nullpointer! " + property + " is not in " + this.getClass().getSimpleName());
                }
            });
        } else throw new NullPointerException("Could not build class. No mandatory schema available!");
    }

    public BaseClass build() {
        this.model = ModelFactory.createDefaultModel();
        this.resource = model.createResource(uri);
        this.resource.addProperty(RDF.type, this.classNamespace);
        try {
            validateSchema();
            customBuild();
            return buildProperties();
        } catch (NullPointerException e) {
            throw e;
        }
    }

    private BaseClass buildProperties() {
        this.attachedProperties.forEach(property -> {
            property.addProperty(this);
        });
        return this;
    }

    protected void customBuild() {
    }

    public void addMandatoryProperty(String propertyName) {
        if (!this.mandatoryProperties.contains(propertyName))
            this.mandatoryProperties.add(PropertyHelper.cleanPropertyName(propertyName));
    }

    public void attachProperty(BaseProperty property) {
        if (property == null) throw new NullPointerException("Attaching Property failed, property is null!");
        else {
            if (!this.attachedProperties.contains(property)) this.attachedProperties.add(property);
            else {
                // What to do with double Properties? - current solution:
                dettachProperty(property);
                this.attachedProperties.add(property);
            }
        }
    }

    public void dettachProperty(BaseProperty property) {
        if (!this.attachedProperties.contains(property)) this.attachedProperties.remove(property);
        else throw new IllegalStateException("Property has not been attached!");
    }

    public Model getModel() {
        return this.model;
    }

    public Resource getResource() {
        return this.resource;
    }
}
