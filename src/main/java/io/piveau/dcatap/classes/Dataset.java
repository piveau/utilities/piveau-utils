package io.piveau.dcatap.classes;

import io.piveau.dcatap.properties.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.DCAT;

import java.time.Instant;
import java.util.ArrayList;
import java.util.UUID;

public class Dataset extends BaseClass {
    private ArrayList<String> keywordList;
    private ArrayList<Distribution> distributionList;

    public Dataset(String uri) {
        super(uri, DCAT.Dataset);
        this.distributionList = new ArrayList<>();
        this.keywordList = new ArrayList<>();
    }

    /**
     * Creates a "random" URL automatically
     */
    public Dataset() {
        super("http://www.piveau.eu/set/data/" + UUID.randomUUID(), DCAT.Dataset);
        this.distributionList = new ArrayList<>();
        this.keywordList = new ArrayList<>();
    }


    /**
     * Override custom build in order to attach ready built graph objects
     */
    @Override
    protected void customBuild() {
        distributionList.forEach(distribution -> {
            if (distribution.getModel() == null)
                throw new NullPointerException("Distribution is null! Build before adding to DCATGraph!");
            else {
                resource.addProperty(DCAT.distribution, distribution.getResource());
                model.add(distribution.getModel());
            }
        });


        this.keywordList.forEach(keyword -> {
            resource.addProperty(DCAT.keyword, keyword);
        });

    }

    /**
     * Mandatory
     */
    public Dataset setTitle(String title) {
        DctTitle dctTitle = new DctTitle(title);
        this.addMandatoryProperty(dctTitle.getClass().getSimpleName());
        this.attachProperty(dctTitle);
        return this;
    }

    public Dataset setTitle(String title, String titleLanguage) {
        DctTitle dctTitle = new DctTitle(title, titleLanguage);
        this.addMandatoryProperty(dctTitle.getClass().getSimpleName());
        this.attachProperty(dctTitle);
        return this;
    }

    public Dataset setDescription(String description) {
        DctDescription dctDescription = new DctDescription(description);
        this.addMandatoryProperty(dctDescription.getClass().getSimpleName());
        this.attachProperty(dctDescription);
        return this;
    }

    public Dataset setDescription(String description, String descriptionLanguage) {
        DctDescription dctDescription = new DctDescription(description, descriptionLanguage);
        this.addMandatoryProperty(dctDescription.getClass().getSimpleName());
        this.attachProperty(dctDescription);
        return this;
    }

    /**
     * Recommended
     */

    public Dataset setContactPoint(String contactPointName, String contactPointEmail) {
        DcatContactPoint dcatContactPoint = new DcatContactPoint(contactPointEmail, contactPointName);
        this.attachProperty(dcatContactPoint);
        return this;
    }

    public Distribution createDistribution() {
        Distribution distribution = new Distribution();
        this.distributionList.add(distribution);
        return distribution;
    }

    public Distribution createDistribution(String uri) {
        Distribution distribution = new Distribution(uri);
        this.distributionList.add(distribution);
        return distribution;
    }

    public Dataset addDistribution(Distribution distribution) {
        distributionList.add(distribution);
        return this;
    }

    public Dataset addKeyword(String keyword) {
        this.keywordList.add(keyword);
        return this;
    }

    public Dataset setPublisher(String publisherName, String publisherHomepage) {
        DctPublisher dctPublisher = new DctPublisher(publisherName, publisherHomepage);
        this.attachProperty(dctPublisher);
        return this;
    }

    public Dataset setSpatial(String spatial) {
        DctSpatial dctSpatialObject = new DctSpatial(spatial);
        this.attachProperty(dctSpatialObject);
        return this;
    }

    public Dataset setTemporal(Instant temporalStartDate, Instant temporalEndDate) {
        DctTemporal temporal = new DctTemporal(temporalStartDate, temporalEndDate);
        this.attachProperty(temporal);
        return this;
    }

    public Dataset setTheme(String theme) {
        DcatTheme dcatThemeObject = new DcatTheme(theme);
        this.attachProperty(dcatThemeObject);
        return this;
    }


    /**
     * Optional
     */

    public Dataset setAccessRights(String accessRight) {
        DctAccessRights accessRightObject = new DctAccessRights(accessRight);
        this.attachProperty(accessRightObject);
        return this;
    }

    public Dataset setCreator(String creator) {
        DctCreator dctCreator = new DctCreator(creator);
        this.attachProperty(dctCreator);
        return this;
    }

    public Dataset setConformsTo(String conformsTo) {
        DctConformsTo dctConformsToObject = new DctConformsTo(conformsTo);
        this.attachProperty(dctConformsToObject);
        return this;
    }

    public Dataset setPage(String foafPage) {
        FoafPage foafPageObject = new FoafPage(foafPage);
        this.attachProperty(foafPageObject);
        return this;
    }

    public Dataset setAccrualPeriodicity(String accuralPeriodicity) {
        DctAccuralPeriodicity dctAccuralPeriodicityObject = new DctAccuralPeriodicity(accuralPeriodicity);
        this.attachProperty(dctAccuralPeriodicityObject);
        return this;
    }

    public Dataset setHasVersion(String hasVersion) {
        DctHasVersion dctHasVersionObject = new DctHasVersion(hasVersion);
        this.attachProperty(dctHasVersionObject);
        return this;
    }

    public Dataset setDCATIdentifier(String identifier) {
        DctIdentifier dctIdentifier = new DctIdentifier(identifier);
        this.attachProperty(dctIdentifier);
        return this;
    }

    public Dataset setIsReferencedBy(String referencedBy) {
        DctIsReferencedBy referencedByObject = new DctIsReferencedBy(referencedBy);
        this.attachProperty(referencedByObject);
        return this;
    }

    public Dataset setIsVersionOf(String isVisionOf) {
        DctIsVersionOf dctIsVersionOf = new DctIsVersionOf(isVisionOf);
        this.attachProperty(dctIsVersionOf);
        return this;
    }

    public Dataset setLandingPage(String landingPage) {
        DcatLandingPage landingPageObject = new DcatLandingPage(landingPage);
        this.attachProperty(landingPageObject);
        return this;
    }

    public Dataset setLanguage(String language) {
        DctLanguage dctLanguage = new DctLanguage(language);
        this.attachProperty(dctLanguage);
        return this;
    }

    public Dataset setADMSIdentifier(String admsIdentifierNotation, String admsIdentifierNotationType, String admsIdentifierLabel) {
        AdmsIdentifier identifier = new AdmsIdentifier(admsIdentifierNotation, admsIdentifierNotationType, admsIdentifierLabel);
        this.attachProperty(identifier);
        return this;
    }

    public Dataset setProvenance(String provernance) {
        DctProvernance provernanceObject = new DctProvernance(provernance);
        this.attachProperty(provernanceObject);
        return this;
    }

    public Dataset setProvenance(String provernance, String provernanceLanguage) {
        DctProvernance provernanceObject = new DctProvernance(provernance, provernanceLanguage);
        this.attachProperty(provernanceObject);
        return this;
    }

    public Dataset setQualifiedAttribution(String foafGivenName, String foafMbox, String foafHomepage) {
        ProvQualifiedAttribution provQualifiedAttribution = new ProvQualifiedAttribution(foafGivenName, foafMbox, foafHomepage);
        this.attachProperty(provQualifiedAttribution);
        return this;
    }

    public Dataset setQualifiedRelation(String qualifiedRelation) {
        DcatQualifiedRelation qualifiedRelationObject = new DcatQualifiedRelation(qualifiedRelation);
        this.attachProperty(qualifiedRelationObject);
        return this;
    }

    public Dataset setRelation(String relation) {
        DctRelation relationObject = new DctRelation(relation);
        this.attachProperty(relationObject);
        return this;
    }

    public Dataset setIssued(Instant date) {
        DctIssued dctIssued = new DctIssued(date);
        this.attachProperty(dctIssued);
        return this;
    }

    public Dataset setSample(String sample) {
        AdmsSample admsSampleObject = new AdmsSample(sample);
        this.attachProperty(admsSampleObject);
        return this;
    }

    public Dataset setSource(String source) {
        DctSource sourceObject = new DctSource(source);
        this.attachProperty(sourceObject);
        return this;
    }

    public Dataset setSpatialResolutionInMeters(String spatialResolutionInMeters) {
        DcatSpatialResolutionInMeters dcatSpatialResolutionInMetersObject = new DcatSpatialResolutionInMeters(spatialResolutionInMeters);
        this.attachProperty(dcatSpatialResolutionInMetersObject);
        return this;
    }

    public Dataset setTemporalResolution(String temporalResolution) {
        DcatTemporalResolution dcatTemporalResolutionObject = new DcatTemporalResolution(temporalResolution);
        this.attachProperty(dcatTemporalResolutionObject);
        return this;
    }

    public Dataset setType(String type) {
        DctType dctTypeObject = new DctType(type);
        this.attachProperty(dctTypeObject);
        return this;
    }

    public Dataset setModified(Instant date) {
        DctModified dctModified = new DctModified(date);
        this.attachProperty(dctModified);
        return this;
    }

    public Dataset setVersionInfo(String versionInfo) {
        OwlVersionInfo versionInfoObject = new OwlVersionInfo(versionInfo);
        this.attachProperty(versionInfoObject);
        return this;
    }

    public Dataset setVersionNote(String versionNote) {
        AdmsVersionNote versionNoteObject = new AdmsVersionNote(versionNote);
        this.attachProperty(versionNoteObject);
        return this;
    }

    public Dataset setVersionNote(String versionNote, String versionLanguage) {
        AdmsVersionNote versionNoteObject = new AdmsVersionNote(versionNote, versionLanguage);
        this.attachProperty(versionNoteObject);
        return this;
    }

    public Dataset setWasGeneratedBy(Instant generatedEndDate, Instant generatedStartDate) {
        ProvWasGeneratedBy provWasGeneratedBy = new ProvWasGeneratedBy(generatedStartDate, generatedEndDate);
        this.attachProperty(provWasGeneratedBy);
        return this;
    }

    public Model getModel() {
        return model;
    }
}
