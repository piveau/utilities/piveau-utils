package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import io.piveau.utils.PropertyHelper;
import org.apache.jena.vocabulary.DCTerms;

public class DctLicense extends BaseProperty {

    public DctLicense(String license) {
        super(license, DCTerms.license);
    }

    @Override
    public void addProperty(BaseClass schema) {
        if (PropertyHelper.isURL(this.value)) {
            schema.getResource().addProperty(namespace, schema.getModel().createResource(this.value));
        } else
            schema.getResource().addProperty(namespace, this.value);
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
        this.addProperty(schema);
    }
}
