package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctCreator extends BaseProperty {

    public DctCreator(String dctCreator) {
        super(dctCreator, DCTerms.creator);
    }
}
