package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctHasPart extends BaseProperty {

    public DctHasPart(String hasPart) {
        super(hasPart, DCTerms.hasPart);
    }
}
