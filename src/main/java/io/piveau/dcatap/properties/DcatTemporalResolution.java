package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;

public class DcatTemporalResolution extends BaseProperty {

    public DcatTemporalResolution(String temporalResolution) {
        super(temporalResolution, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#temporalResolution"));
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource()
                .addProperty(this.namespace, ResourceFactory.createTypedLiteral(this.value, XSDDatatype.XSDduration));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
