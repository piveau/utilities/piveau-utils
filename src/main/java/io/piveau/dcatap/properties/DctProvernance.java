package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

public class DctProvernance extends BaseProperty {
    public DctProvernance(String provernance) {
        super(provernance, DCTerms.provenance);
    }

    public DctProvernance(String provernance, String language) {
        super(provernance, language, DCAT.landingPage);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource()
                .addProperty(namespace, schema.getModel().createResource()
                        .addProperty(RDF.type, DCTerms.ProvenanceStatement)
                        .addProperty(RDFS.label, value));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
        schema.getResource()
                .addProperty(namespace, schema.getModel().createResource()
                        .addProperty(RDF.type, DCTerms.ProvenanceStatement)
                        .addProperty(RDFS.label, value, language));
    }
}
