package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctIsVersionOf extends BaseProperty {

    public DctIsVersionOf(String versionOf) {
        super(versionOf, DCTerms.isVersionOf);
    }
}
