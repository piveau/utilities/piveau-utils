package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctIsPartOf extends BaseProperty {

    public DctIsPartOf(String isPartOf) {
        super(isPartOf, DCTerms.isPartOf);
    }
}
