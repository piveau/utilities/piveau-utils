package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctSource extends BaseProperty {

    public DctSource(String source) {
        super(source, DCTerms.source);
    }
}
