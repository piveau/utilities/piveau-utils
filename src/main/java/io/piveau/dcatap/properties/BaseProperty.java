package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.rdf.model.Property;

public class BaseProperty {
    public String value;
    public Property namespace;
    public String language;

    public BaseProperty() {
    }

    public BaseProperty(String value, Property namespace) {
        this.value = value;
        this.namespace = namespace;
        this.language = "en";
    }

    public BaseProperty(String value, String language, Property namespace) {
        this.value = value;
        this.namespace = namespace;
        this.language = language;
    }

    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(this.namespace, schema.getModel().createResource(value));
    }

    public void addPropertyLanguage(BaseClass schema) {
        schema.getResource().addProperty(this.namespace, value, language);
    }
}
