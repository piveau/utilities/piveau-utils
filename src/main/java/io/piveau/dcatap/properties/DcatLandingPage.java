package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCAT;

public class DcatLandingPage extends BaseProperty {

    public DcatLandingPage(String landingPage) {
        super(landingPage, DCAT.landingPage);
    }
}
