package io.piveau.dcatap.properties;

import org.apache.jena.rdf.model.ModelFactory;

public class DcatAccessService extends BaseProperty {

    public DcatAccessService(String accessService) {
        super(accessService, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#accessService"));
    }
}
