package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;

import java.time.Instant;

public class DctTemporal extends BaseProperty {
    Instant temporalStartDate, temporalEndDate;

    public DctTemporal(Instant temporalStartDate, Instant temporalEndDate) {
        super(null, DCTerms.temporal);
        this.temporalStartDate = temporalStartDate;
        this.temporalEndDate = temporalEndDate;
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource()
                .addProperty(namespace, schema.getModel().createResource()
                        .addProperty(RDF.type, DCTerms.PeriodOfTime)
                        .addProperty(schema.getModel().createProperty("http://www.w3.org/ns/dcat#endDate"), ResourceFactory.createTypedLiteral(temporalEndDate.toString(), XSDDatatype.XSDdateTime))
                        .addProperty(schema.getModel().createProperty("http://www.w3.org/ns/dcat#startDate"), ResourceFactory.createTypedLiteral(temporalStartDate.toString(), XSDDatatype.XSDdateTime)));

    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
