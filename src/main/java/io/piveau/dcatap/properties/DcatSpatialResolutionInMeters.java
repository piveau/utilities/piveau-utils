package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;

public class DcatSpatialResolutionInMeters extends BaseProperty {

    public DcatSpatialResolutionInMeters(String spatialResolutionInMeters) {
        super(spatialResolutionInMeters, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#spatialResolutionInMeters"));
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().
                addProperty(this.namespace, ResourceFactory.createTypedLiteral(this.value, XSDDatatype.XSDdecimal));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
