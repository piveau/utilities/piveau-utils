package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDF;

public class ProvQualifiedAttribution extends BaseProperty {
    private String foafGivenName, foafMbox, foafHomepage;

    public ProvQualifiedAttribution(String foafGivenName, String foafMbox, String foafHomepage) {
        super(null, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/prov#qualifiedAttribution"));
        this.foafGivenName = foafGivenName;
        this.foafMbox = foafMbox;
        this.foafHomepage = foafHomepage;
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource()
                .addProperty(namespace, schema.getModel().createResource()
                        .addProperty(RDF.type, schema.getModel().createProperty("http://www.w3.org/ns/prov#Attribution"))
                        .addProperty(schema.getModel().createProperty("http://www.w3.org/ns/prov#agent"), schema.getModel().createResource()
                                .addProperty(RDF.type, schema.getModel().createProperty("http://www.w3.org/ns/prov#Agent"))
                                .addProperty(RDF.type, schema.getModel().createProperty("http://www.w3.org/ns/prov#Person"))
                                .addProperty(FOAF.givenName, ResourceFactory.createTypedLiteral(foafGivenName, XSDDatatype.XSDstring))
                                .addProperty(FOAF.mbox, schema.getModel().createResource(foafMbox))
                                .addProperty(FOAF.homepage, schema.getModel().createResource(foafHomepage))));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
