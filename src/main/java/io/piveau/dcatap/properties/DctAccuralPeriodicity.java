package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctAccuralPeriodicity extends BaseProperty {

    public DctAccuralPeriodicity(String accrualPeriodicity) {
        super(accrualPeriodicity, DCTerms.accrualPeriodicity);
    }
}
