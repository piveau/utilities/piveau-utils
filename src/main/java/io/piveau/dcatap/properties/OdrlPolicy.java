package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.vocabulary.RDF;

public class OdrlPolicy extends BaseProperty {

    public OdrlPolicy(String policy) {
        super(policy, ModelFactory.createDefaultModel().createProperty("https://www.w3.org/TR/odrl-vocab/#hasPolicy"));
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(this.namespace, schema.getModel().createResource()
                .addProperty(RDF.type, schema.getModel().createProperty("https://www.w3.org/TR/odrl-vocab/#Policy"))
                .addProperty(schema.getModel().createProperty("https://www.w3.org/TR/odrl-vocab/#permission"), schema.getModel().createResource()
                        .addProperty(schema.getModel().createProperty("https://www.w3.org/TR/odrl-vocab/#action"), value)));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
