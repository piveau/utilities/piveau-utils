package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCAT;

public class DcatDataset extends BaseProperty {

    public DcatDataset(String dataset) {
        super(dataset, DCAT.dataset);
    }

}
