package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCAT;

public class DcatAccessURL extends BaseProperty {

    public DcatAccessURL(String accessURL) {
        super(accessURL, DCAT.accessURL);
    }
}
