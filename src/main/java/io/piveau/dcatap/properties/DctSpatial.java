package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctSpatial extends BaseProperty {

    public DctSpatial(String spatial) {
        super(spatial, DCTerms.spatial);
    }
}
