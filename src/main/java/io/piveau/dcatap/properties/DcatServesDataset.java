package io.piveau.dcatap.properties;

import org.apache.jena.rdf.model.ModelFactory;

public class DcatServesDataset extends BaseProperty {

    public DcatServesDataset(String endPointDescription) {
        super(endPointDescription, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#servesDataset"));
    }
}
