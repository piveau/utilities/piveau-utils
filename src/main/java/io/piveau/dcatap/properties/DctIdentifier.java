package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCTerms;

public class DctIdentifier extends BaseProperty {

    public DctIdentifier(String identifier) {
        super(identifier, DCTerms.identifier);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(namespace, value);
    }
}
