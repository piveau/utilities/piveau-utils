package io.piveau.dcatap.properties;

import org.apache.jena.rdf.model.ModelFactory;

public class DcatCatalogue extends BaseProperty {

    public DcatCatalogue(String dcatCatalogue) {
        super(dcatCatalogue, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#catalog"));
    }
}
