package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCTerms;

public class DctDescription extends BaseProperty {

    public DctDescription(String description) {
        super(description, DCTerms.description);
    }

    public DctDescription(String description, String language) {
        super(description, language, DCTerms.description);
    }

    /**
     * Override to keep default language
     *
     * @param schema
     */
    @Override
    public void addProperty(BaseClass schema) {
        this.addPropertyLanguage(schema);
    }
}
