package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctRelation extends BaseProperty {

    public DctRelation(String dctRelation) {
        super(dctRelation, DCTerms.relation);
    }
}
