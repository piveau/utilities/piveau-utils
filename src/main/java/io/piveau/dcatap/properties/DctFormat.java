package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import io.piveau.utils.PropertyHelper;
import org.apache.jena.vocabulary.DCTerms;

public class DctFormat extends BaseProperty {

    public DctFormat(String format) {
        super(format, DCTerms.format);
    }

    @Override
    public void addProperty(BaseClass schema) {
        if (PropertyHelper.isURL(this.value)) {
            schema.getResource().addProperty(this.namespace, schema.getModel().createResource(this.value));
        } else
            schema.getResource().addProperty(this.namespace, this.value);
    }
}
