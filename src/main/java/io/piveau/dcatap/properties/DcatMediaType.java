package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCAT;

public class DcatMediaType extends BaseProperty {

    public DcatMediaType(String mediaType) {
        super(mediaType, DCAT.mediaType);
    }
}
