package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

public class DctConformsTo extends BaseProperty {

    public DctConformsTo(String conformsTo) {
        super(conformsTo, DCTerms.conformsTo);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource()
                .addProperty(this.namespace, schema.getModel().createResource()
                        .addProperty(RDF.type, DCTerms.Standard)
                        .addProperty(RDFS.label, this.value));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
