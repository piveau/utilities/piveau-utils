package io.piveau.dcatap.properties;

import org.apache.jena.sparql.vocabulary.FOAF;

public class FoafHomepage extends BaseProperty {

    public FoafHomepage(String homepage) {
        super(homepage, FOAF.homepage);
    }
}
