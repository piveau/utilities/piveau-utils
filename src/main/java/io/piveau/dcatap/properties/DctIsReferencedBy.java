package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctIsReferencedBy extends BaseProperty {

    public DctIsReferencedBy(String referencedBy) {
        super(referencedBy, DCTerms.isReferencedBy);
    }
}
