package io.piveau.dcatap.properties;

import org.apache.jena.rdf.model.ModelFactory;

public class DcatEndPointURL extends BaseProperty {

    public DcatEndPointURL(String endPointURL) {
        super(endPointURL, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#endPointURL"));
    }
}
