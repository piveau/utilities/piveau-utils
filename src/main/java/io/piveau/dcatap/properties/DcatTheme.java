package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCAT;

public class DcatTheme extends BaseProperty {

    public DcatTheme(String theme) {
        super(theme, DCAT.theme);
    }
}
