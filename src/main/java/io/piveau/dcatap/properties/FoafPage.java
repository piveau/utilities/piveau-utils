package io.piveau.dcatap.properties;

import org.apache.jena.sparql.vocabulary.FOAF;

public class FoafPage extends BaseProperty {

    public FoafPage(String foafPage) {
        super(foafPage, FOAF.page);
    }
}
