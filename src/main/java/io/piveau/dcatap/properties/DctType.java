package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCTerms;

public class DctType extends BaseProperty {

    public DctType(String type) {
        super(type, DCTerms.type);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(namespace, value);
    }
}
