package io.piveau.security;

public class KeycloakTokenServerConfig extends TokenServerConfig {

    @Override
    TokenServer getTokenServer() {
        return new KeycloakTokenServer(this);
    }

}
