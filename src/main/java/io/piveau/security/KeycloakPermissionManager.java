package io.piveau.security;

import io.piveau.state.SimpleState;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/*
    Permission Manager, used in the access control feature in hub-repo and hub-search
 */
public class KeycloakPermissionManager {

    private static final Logger log = LoggerFactory.getLogger(KeycloakPermissionManager.class);
    // TODO: make value configurable
    private static final String GLOBAL_RESOURCE_NAME = "0_global";


    public KeycloakPermissionManager(SimpleState state) {
        this.permissionHierarchy = new HashMap<>();
        this.state = state;
        if (state != null) {
            keycloakPermissions = state.getPermissions();
        }
    }

    // TODO: temporary constructor, remove if not required
    public KeycloakPermissionManager(JsonArray permissions) {
        this.permissionHierarchy = new HashMap<>();
        this.keycloakPermissions = permissions;
        this.state = null;
    }

    public enum PERMISSION {
        // Level: catalog, dataset
        // view datasets that are published (publication_state = published)
        DATASET_VIEW_PUBLISHED("dataset:view_published"),
        // view drafts
        DATASET_VIEW_DRAFT("dataset:view_draft"),
        // Update Payload and update visibility using action or param in endpoint
        DATASET_UPDATE("dataset:update"),
        // Delete dataset
        DATASET_DELETE("dataset:delete"),
        // Change publication status of dataset (published -> draft and draft -> published)
        // regardless of whether the publication status is represented by a catalog record entry or location primary/shadow TS
        DATASET_PUBLISH("dataset:publish"),

        // Level: catalog
        // Create dataset and set visibility
        DATASET_CREATE("dataset:create"),
        // view catalog data (not the containing datasets)
        CATALOG_VIEW("catalog:view"),
        // update catalog data
        CATALOG_UPDATE("catalog:update"),
        // Delete Catalog
        CATALOG_DELETE("catalog:delete"),

        // Level: global, catalog
        // create catalog
        CATALOG_CREATE("catalog:create")

        ;

        private final String label;

        PERMISSION(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public static PERMISSION fromString(String text) {
            for (PERMISSION b : PERMISSION.values()) {
                if (b.label.equalsIgnoreCase(text)) {
                    return b;
                }
            }
            return null;
        }

        public boolean equalsIgnoreCase(String other) {
            return label.equalsIgnoreCase(other);
        }

        @Override
        public String toString() {
            return label;
        }
    }

    private final Map<PERMISSION, List<Record>> permissionHierarchy;
    private final SimpleState state;
    private JsonArray keycloakPermissions = null;

    /*
        get all permissions on the path to entity
        Assumption: in KC the full path is given. for catalogs you need the super catalog if exists
     */
    public Set<PERMISSION> getPermissionsForEntity(String entityPath, boolean includeGlobalOperatorRole) {
        Set<PERMISSION> result = new HashSet<>();

        // anonymous user or user without permissions
        if (keycloakPermissions == null) return result;

        keycloakPermissions.stream()
            .filter(JsonObject.class::isInstance)
            .map(JsonObject.class::cast)
            .forEach(permission -> {
                String resourceName = permission.getString("rsname", "");
                if (entityPath.startsWith(resourceName)
                        || (resourceName.equals(GLOBAL_RESOURCE_NAME) && includeGlobalOperatorRole)) {
                    JsonArray scopes = permission.getJsonArray("scopes", new JsonArray());
                    for (Object scope : scopes) {
                        if (PERMISSION.fromString(scope.toString()) != null) {
                            // duplicates are removed here, because it is a set
                            result.add(PERMISSION.fromString(scope.toString()));
                        }
                    }
                }});

        return  result;
    }

    public boolean containsAllRequiredPermissions (String entityPath, Set<PERMISSION> requiredPermissions, boolean includeOperator) {
        Set<KeycloakPermissionManager.PERMISSION> currentPermissions = getPermissionsForEntity(entityPath, includeOperator);
        return currentPermissions.containsAll(requiredPermissions);
    }

    public boolean isAnonymousUser(){
        return !state.hasUser();
    }
}
