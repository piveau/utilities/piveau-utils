package io.piveau.security;

import io.vertx.core.json.JsonObject;

public class PiveauAuthConfig {

    private String issuer = "";
    private String clientId = "";
    private String clientSecret = "";
    private String customSecret = "";

    private TokenServerConfig tokenServerConfig;

    public PiveauAuthConfig() {
        tokenServerConfig = TokenServerConfig.create(new JsonObject());
    }

    public PiveauAuthConfig(JsonObject config) {
        issuer = config.getString("issuer", issuer);
        clientId = config.getString("clientId", clientId);
        clientSecret = config.getString("clientSecret", clientSecret);
        customSecret = config.getString("customSecret", customSecret);

        JsonObject tsConfig = config.getJsonObject("tokenServerConfig", new JsonObject());
        tokenServerConfig = TokenServerConfig.create(tsConfig);
    }

    public String getIssuer() {
        return issuer;
    }

    public PiveauAuthConfig setIssuer(String issuer) {
        this.issuer = issuer;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public PiveauAuthConfig setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getCustomSecret() {
        return customSecret;
    }

    public void setCustomSecret(String customSecret) {
        this.customSecret = customSecret;
    }

    public PiveauAuthConfig setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
        return this;
    }

    public TokenServerConfig getTokenServerConfig() {
        return tokenServerConfig;
    }

    public PiveauAuthConfig setTokenServerConfig(TokenServerConfig tokenServerConfig) {
        this.tokenServerConfig = tokenServerConfig;
        return this;
    }

}
