/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.security;

import io.vertx.core.*;
import io.vertx.ext.auth.*;
import io.vertx.ext.web.*;
import io.vertx.ext.web.handler.impl.*;

public class DummyAuthenticationHandler implements AuthenticationHandlerInternal {

    @Override
    public void authenticate(RoutingContext routingContext, Handler<AsyncResult<User>> handler) {
        routingContext.response().setStatusCode(401).end("Bearer Token support is not activated.");
    }

    @Override
    public void handle(RoutingContext routingContext) {
        routingContext.next();
    }
}
