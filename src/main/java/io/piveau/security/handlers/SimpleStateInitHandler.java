package io.piveau.security.handlers;

import io.piveau.state.SimpleState;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public class SimpleStateInitHandler implements Handler<RoutingContext> {
    @Override
    public void handle(RoutingContext context) {
        SimpleState state = new SimpleState(context);
        context.put(SimpleState.STATE_KEY, state);
        context.next();
    }
}
