package io.piveau.state;

import io.piveau.vocabularies.vocabulary.PV;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import org.apache.jena.rdf.model.Resource;

import java.util.Map;

@DataObject
public class SimpleState {
    public static String STATE_KEY = "state";
    public static String PERMISSIONS_KEY = "permissions";
    public static String DATASET_PART_COMPLETE_DATASET = "completeDataset";
    public static String DATASET_PART_FILTER_DISTRIBUTION_URLS = "filterDistributionUrls";
    public static String OPERATION_UPDATE_DATASET = "updateDataset";
    public static String OPERATION_CREATE_DATASET = "createDataset";
    public static String OPERATION_UPDATE_CATALOGUE = "updateCatalogue";
    public static String OPERATION_CREATE_CATALOGUE = "createCatalogue";

    private static final String PROPERTY_USER_PRINCIPAL = "userPrincipal";
    private static final String PROPERTY_USER_ATTRIBUTES = "userAttributes";
    private static final String PROPERTY_PERMISSIONS = "permissions";
    private static final String PROPERTY_VISIBILITY = "visibility";
    private static final String PROPERTY_ENTITY_OPERATION = "datasetOperation";
    private static final String PROPERTY_ACTION_METHOD = "actionMethod";
    private static final String PROPERTY_DATASET_PART = "datasetPart";

    // RoutingContext is not preserved during serialization and deserialization. It is accessible before passing to the services
    private RoutingContext routingContext;
    private User user;
    private JsonArray permissions;
    private Resource visibility;
    private String entityOperation;
    private String actionMethod;
    private String datasetPart;
    // path to entity if already collected

    // TODO: provide better solution
    private static final Map<String, Resource> stringToVisibility = Map.of(
            "https://piveau.eu/ns/voc#public", PV.visibilityPublic,
            "https://piveau.eu/ns/voc#private", PV.visibilityPrivate,
            "https://piveau.eu/ns/voc#restricted", PV.visibilityRestricted);

    public SimpleState() {}

    public SimpleState(RoutingContext ctx) {
        this.routingContext = ctx;
        this.permissions = ctx.get(PERMISSIONS_KEY);
        this.user = ctx.user();
    }

    public SimpleState(User user) {
        this.user = user;
    }

    public SimpleState(JsonObject json) {
        this.permissions = json.getJsonArray(PROPERTY_PERMISSIONS);
        this.entityOperation = json.getString(PROPERTY_ENTITY_OPERATION);
        this.actionMethod = json.getString(PROPERTY_ACTION_METHOD);
        this.datasetPart = json.getString(PROPERTY_DATASET_PART);

        if (json.getString(PROPERTY_VISIBILITY) != null) {
            this.visibility = stringToVisibility.get(json.getString(PROPERTY_VISIBILITY));
        }

        if (json.getJsonObject(PROPERTY_USER_PRINCIPAL) != null) {
            this.user = User.create(
                    json.getJsonObject(PROPERTY_USER_PRINCIPAL),
                    json.getJsonObject(PROPERTY_USER_ATTRIBUTES));
        }
    }

    public JsonObject toJson() {
        return new JsonObject()
            .put(PROPERTY_PERMISSIONS, permissions)
            .put(PROPERTY_USER_PRINCIPAL, (user != null) ? user.principal() : null)
            .put(PROPERTY_USER_ATTRIBUTES, (user != null) ? user.attributes() : null)
            .put(PROPERTY_ENTITY_OPERATION, entityOperation)
            .put(PROPERTY_ACTION_METHOD, actionMethod)
            .put(PROPERTY_DATASET_PART, datasetPart)
            .put(PROPERTY_VISIBILITY, (visibility != null) ? visibility.toString() : null)
            ;
    }

    public String getUserID() {
        if (user != null) {
            return user.attributes().getString("sub");
        } else {
            return null;
        }
    }

    public User getUser() {
        return user;
    }

    public boolean hasUser() {
        return user != null;
    }

    public Resource getVisibility() {
        return visibility;
    }

    public String getEntityOperation() { return entityOperation; }

    public JsonArray getPermissions() { return permissions; }

    public String getActionMethod() { return actionMethod; }

    public RoutingContext getRoutingContext() { return routingContext; }

    public String getDatasetPart() { return datasetPart; }

    public SimpleState setEntityOperation(String operation) {
        this.entityOperation = operation;
        return this;
    }

    public SimpleState setUser(User user) {
        this.user = user;
        return this;
    }

    public SimpleState setPermissions(JsonArray permissions) {
        this.permissions = permissions;
        return this;
    }

    public SimpleState setVisibility(Resource visibility) {
        this.visibility = visibility;
        return this;
    }

    public SimpleState setDatasetPart(String datasetPart) {
        this.datasetPart = datasetPart;
        return this;
    }

    public SimpleState setActionMethod(String actionMethod) {
        this.actionMethod = actionMethod;
        return this;
    }

    public SimpleState setRoutingContext(RoutingContext context) {
        this.routingContext = context;
        return this;
    }


}
