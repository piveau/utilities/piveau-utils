package io.piveau.state;

import io.piveau.state.SimpleState;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

public class SimpleStateMessageCodec implements MessageCodec<SimpleState, SimpleState> {

    @Override
    public void encodeToWire(Buffer buffer, SimpleState entries) { }

    @Override
    public SimpleState decodeFromWire(int i, Buffer buffer) {
        return null;
    }

    @Override
    public SimpleState transform(SimpleState state) {
        return state;
    }

    @Override
    public String name() {
        return getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }

}
