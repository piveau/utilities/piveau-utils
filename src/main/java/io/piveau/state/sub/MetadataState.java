package io.piveau.state.sub;

import io.piveau.state.State;

import java.util.List;

public class MetadataState implements SubState {

    private State parent;


    @Override
    public State getParent() {
        return parent;
    }

    @Override
    public SubState setParent(State parent) {
        this.parent = parent;
        return this;
    }


    private record Dataset(String id, String catalog, Boolean draft, Boolean restricted) {
    }

    private record Catalog(String id, Boolean restricted) {
    }

    private List<Dataset> datasets;
    private List<Catalog> catalogs;

    public List<Dataset> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<Dataset> datasets) {
        this.datasets = datasets;
    }

    public List<Catalog> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(List<Catalog> catalogs) {
        this.catalogs = catalogs;
    }

    public Boolean isDraft(String datasetId) {
        return datasets.stream()
                .filter(d -> d.id.equals(datasetId))
                .findFirst()
                .map(d -> d.draft)
                .orElse(false);
    }

    public Boolean isRestricted(String datasetId) {
        return datasets.stream()
                .filter(d -> d.id.equals(datasetId))
                .findFirst()
                .map(d -> d.restricted || isCatalogRestricted(d.catalog))
                .orElse(false);
    }

    public Boolean isCatalogRestricted(String catalogId) {
        return catalogs.stream()
                .filter(c -> c.id.equals(catalogId))
                .findFirst()
                .map(c -> c.restricted)
                .orElse(false);
    }

}
