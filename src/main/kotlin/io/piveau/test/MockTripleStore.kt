@file:JvmName("MockTripleStore")
@file:JvmMultifileClass

package io.piveau.test

import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.presentAs
import io.piveau.rdf.toModel
import io.piveau.utils.JenaUtils
import io.piveau.vocabularies.readTurtleResource
import io.vertx.core.*
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import org.apache.http.HttpStatus
import org.apache.jena.query.*
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.sparql.resultset.ResultsFormat
import org.apache.jena.update.UpdateAction
import org.apache.jena.update.UpdateFactory
import org.slf4j.LoggerFactory
import java.io.ByteArrayOutputStream

class MockTripleStore : AbstractVerticle() {

    companion object {
        private val log = LoggerFactory.getLogger(MockTripleStore::class.java)
        private const val dataEndpoint = "/data"
        private const val queryEndpoint = "/query"
        private const val updateEndpoint = "/update"

        private const val GRAPH_MISSING_ERROR = "Param 'graph' is missing or empty"

        @JvmStatic
        fun getDefaultConfig(): JsonObject = JsonObject()
            .put("address", "http://localhost:9096")
            .put("queryEndpoint", queryEndpoint)
            .put("queryAuthEndpoint", updateEndpoint)
            .put("graphEndpoint", dataEndpoint)
            .put("graphAuthEndpoint", dataEndpoint)

    }

    private var storeDataset = DatasetFactory.createTxnMem()


    override fun start(startPromise: Promise<Void>) {

        val config = config()
        val router = Router.router(vertx)
        router[queryEndpoint]
            .handler(this::query)
        router.put(dataEndpoint).handler(BodyHandler.create())
            .handler(this::putData)
        router[dataEndpoint]
            .handler(this::getData)
        router.delete(dataEndpoint)
            .handler(this::deleteData)
        router.post(updateEndpoint).handler(BodyHandler.create())
            .handler(this::update)
        router.post(dataEndpoint).handler(BodyHandler.create())
            .handler(this::postData)

        val httpServerOptions = HttpServerOptions()
            .setIdleTimeout(60) //High timeout for debugging

        httpServerOptions.setTcpUserTimeout(1000)

        vertx.createHttpServer(httpServerOptions).requestHandler(router).listen(config.getInteger("port", 9096)) {
            when (it.succeeded()) {
                true -> startPromise.complete()
                else -> startPromise.fail(it.cause())
            }
        }
    }

    fun query(context: RoutingContext) {

        val mimeType = context.parsedHeaders().accept().firstOrNull()?.value()

        val queryParam = context.queryParam("query").firstOrNull { it.isNotBlank() }
        if (queryParam == null) {
            context.response().setStatusCode(HttpStatus.SC_BAD_REQUEST).end("Param 'query' is missing or empty")
            return
        }

        val query = QueryFactory.create(queryParam)

        val fmt = context.parsedHeaders().accept().firstOrNull()?.subComponent()?.let { ResultsFormat.lookup(it) }
            ?: ResultsFormat.FMT_RS_JSON

        try {
            val queryExecution = QueryExecutionFactory.create(query, storeDataset)
            context.response().statusCode = HttpStatus.SC_OK

            when (query.queryType()) {
                QueryType.SELECT -> {
                    try {
                        val resultSet = queryExecution.execSelect()
                        val out = ByteArrayOutputStream()
                        ResultSetFormatter.output(out, resultSet, fmt)
                        context.response().end(out.toString())

                    } catch (exception: Exception) {
                        println(exception)
                    }
                }
                QueryType.CONSTRUCT -> context.response().end(
                    queryExecution.execConstruct().presentAs(mimeType ?: RDFMimeTypes.RDFXML)
                )
                QueryType.ASK -> context.response().end(
                    JsonObject().put("boolean", queryExecution.execAsk()).encodePrettily()
                )
                QueryType.DESCRIBE -> context.response().end(
                    queryExecution.execDescribe().presentAs(mimeType ?: RDFMimeTypes.RDFXML)
                )
                else -> {
                    queryExecution.abort()
                    context.response().setStatusCode(HttpStatus.SC_BAD_REQUEST)
                        .end("Query error. Unknown Query Type: ${query.queryType().name}")
                }
            }

        } catch (exception: Exception) {
            context.response().statusCode = HttpStatus.SC_INTERNAL_SERVER_ERROR
            context.response().end(exception.message)
        }

    }

    private fun update(context: RoutingContext) {
//        val queryParam = context.queryParam("query").firstOrNull { it.isNotBlank() }
//        if (queryParam == null) {
//            context.response().setStatusCode(HttpStatus.SC_BAD_REQUEST).end("Param 'query' is missing or empty")
//            return
//        }

        val queryParam = context.body().asString()

        try {
            val updateRequest = UpdateFactory.create(queryParam)
            UpdateAction.execute(updateRequest, storeDataset)
            context.response().setStatusCode(HttpStatus.SC_OK).end()
        } catch (exception: Exception) {
            context.response().statusCode = HttpStatus.SC_INTERNAL_SERVER_ERROR
            context.response().end(exception.message)
        }
    }

    private fun getData(context: RoutingContext) {

        val mimeType = context.parsedHeaders().accept().firstOrNull()?.value()

        val graphUri = context.queryParam("graph").firstOrNull { it.isNotBlank() }
        if (graphUri == null) {
            context.response().setStatusCode(HttpStatus.SC_BAD_REQUEST).end(GRAPH_MISSING_ERROR)
            return
        }

        val model = storeDataset.getNamedModel(graphUri)
        if (model == null || model.isEmpty) {
            context.response().setStatusCode(HttpStatus.SC_NOT_FOUND).end()
            return
        }
        log.info("Returning graph with uri $graphUri")
        context.response().setStatusCode(HttpStatus.SC_OK).end(
            JenaUtils.write(
                model,
                mimeType
            )
        )
    }

    private fun putData(context: RoutingContext) {

        val graphUri = context.queryParam("graph").firstOrNull { it.isNotBlank() }
        if (graphUri == null) {
            context.response().setStatusCode(HttpStatus.SC_BAD_REQUEST).end(GRAPH_MISSING_ERROR)
            return
        }

        val contentType = context.parsedHeaders().contentType().value()

        //Return HttpStatus.SC_CREATED (=201) when new graph, HttpStatus.SC_OK(=200) when graph with this uri already exists
        val model = context.body().asString().toByteArray().toModel(contentType)
        if (storeDataset.containsNamedModel(graphUri)) {
            log.info("Graph with uri $graphUri already exists. Overwriting.")
            storeDataset.replaceNamedModel(graphUri, model)
            context.response().setStatusCode(HttpStatus.SC_OK).end()
        } else {
            storeDataset.addNamedModel(graphUri, model)
            context.response().setStatusCode(HttpStatus.SC_CREATED).end()
        }
    }

    private fun postData(context: RoutingContext) {

        val graphUri = context.queryParam("graph").firstOrNull { it.isNotBlank() }
        if (graphUri == null) {
            context.response().setStatusCode(HttpStatus.SC_BAD_REQUEST).end(GRAPH_MISSING_ERROR)
            return
        }

        val contentType = context.parsedHeaders().contentType().value()

        //Return HttpStatus.SC_CREATED (=201) when new graph, HttpStatus.SC_OK(=200) when graph with this uri already exists
        val model = context.body().asString().toByteArray().toModel(contentType)
        if (storeDataset.containsNamedModel(graphUri)) {
            val updatedModel = storeDataset.getNamedModel(graphUri).add(model)
            storeDataset.replaceNamedModel(graphUri, updatedModel)
            context.response().setStatusCode(HttpStatus.SC_OK).end()
            if (storeDataset.containsNamedModel(graphUri)) {
                log.info("Graph with uri $graphUri overwritten.")
            }
        } else {
            log.info("Graph with uri $graphUri does not exist.")
            context.response().setStatusCode(HttpStatus.SC_NOT_FOUND).end()
        }
    }

    private fun deleteData(context: RoutingContext) {
        val graphUri = context.queryParam("graph").firstOrNull { it.isNotBlank() }
        if (graphUri == null) {
            context.response().setStatusCode(HttpStatus.SC_BAD_REQUEST).end(GRAPH_MISSING_ERROR)
            return
        }

        if (!storeDataset.containsNamedModel(graphUri)) {
            context.response().setStatusCode(HttpStatus.SC_NOT_FOUND).end()
            return
        }
        storeDataset.removeNamedModel(graphUri)
        context.response().setStatusCode(HttpStatus.SC_OK).end()
    }

    fun deploy(vertx: Vertx): Future<String> = vertx.deployVerticle(
        this, DeploymentOptions().setThreadingModel(
            ThreadingModel.WORKER
        )
    )

    fun loadGraph(graphName: String, filename: String): MockTripleStore {
        storeDataset.addNamedModel(graphName, ModelFactory.createDefaultModel().readTurtleResource(filename))
        return this
    }

    fun loadGraph(graphName: String, model: Model): MockTripleStore {
        storeDataset.addNamedModel(graphName, model)
        return this
    }

    fun loadModel(filename: String): MockTripleStore {
        storeDataset.defaultModel.add(ModelFactory.createDefaultModel().readTurtleResource(filename))
        return this
    }

    fun loadModel(model: Model): MockTripleStore {
        storeDataset.defaultModel.add(model)
        return this
    }

    fun reset(): MockTripleStore {
        storeDataset = DatasetFactory.createTxnMem()
        return this
    }
}
