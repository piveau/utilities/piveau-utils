package io.piveau.vocabularies

import io.piveau.vocabularies.vocabulary.AT
import org.apache.jena.vocabulary.DC_11
import org.apache.jena.vocabulary.RDFS
import org.apache.jena.vocabulary.SKOS

open class AuthorityTable(uriRef: String, localResource: String, remote: String? = null) : ConceptScheme(uriRef, localResource, remote) {
    override val id: String
        get() = scheme.getProperty(AT.table_id)?.literal?.lexicalForm
            ?: super.id

    fun label(lang: String): String? = prefLabel(lang) ?: altLabel(lang)

    fun prefLabel(lang: String): String? = scheme.getProperty(SKOS.prefLabel, lang)?.literal?.string
        ?: scheme.getProperty(SKOS.prefLabel)?.literal?.lexicalForm

    fun altLabel(lang: String): String? = scheme.getProperty(SKOS.altLabel, lang)?.literal?.string
        ?: scheme.getProperty(SKOS.altLabel)?.literal?.lexicalForm

    val label: String
        get() = prefLabel("en") ?: altLabel("en") ?: ""

    fun getLabel(lang: String): String = prefLabel(lang) ?: altLabel(lang) ?: ""

    fun conceptFromIdentifier(identifier: String): Concept? =
        ontology.listResourcesWithProperty(DC_11.identifier, identifier)
            .toList()
            .map { Concept(it.asResource(), this) }
            .firstOrNull()

    fun conceptFromLabel(value: String, lang: String = "en"): Concept? =
        ontology.listResourcesWithProperty(RDFS.label, ontology.createLiteral(value, lang))
            .toList()
            .map { Concept(it.asResource(), this) }
            .firstOrNull()

    fun findConcept(match: String): Concept? = conceptFromIdentifier(match) ?: conceptFromLabel(match)

}