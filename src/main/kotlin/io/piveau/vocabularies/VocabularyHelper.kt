package io.piveau.vocabularies

import io.piveau.dcatap.TripleStore
import io.piveau.json.putIfNotEmpty
import io.piveau.json.putIfNotNull
import io.piveau.vocabularies.vocabulary.*
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.DC_11
import org.apache.jena.vocabulary.RDF
import org.apache.jena.vocabulary.SKOS
import java.text.Normalizer

fun normalize(id: String?): String {
    val normalized = Normalizer.normalize(id, Normalizer.Form.NFKD)
    //remove all '%'
    //replace non-word-characters with '-'
    //then combine multiple '-' into one
    return normalized.replace("%".toRegex(), "")
        .replace("\\W".toRegex(), "-")
        .replace("-+".toRegex(), "-")
}

class VocabularyHelper internal constructor(private val tripleStore: TripleStore) {

    fun getConceptScheme(vocabulary: String, concept: String): Future<List<Resource>> =
        selectAsResourceList(vocabulary, "?conceptScheme", "<$concept> <${SKOS.inScheme}> ?conceptScheme")

    fun getIdentifier(vocabulary: String, concept: String): Future<String> =
        selectAsLexicalForm(vocabulary, "?identifier", "<$concept> <${DC_11.identifier}> ?identifier")

    fun getExactMatch(vocabulary: String, concept: String): Future<Resource> =
        selectAsResource(vocabulary, "?exactMatch", "<$concept> <${SKOS.exactMatch}> ?exactMatch")

    fun getPrefLabels(vocabulary: String, concept: String): Future<List<Pair<String, String>>> =
        selectAsLiteralPairList(vocabulary, "?prefLabel", "<$concept> <${SKOS.prefLabel}> ?prefLabel")

    fun getPrefLabel(vocabulary: String, concept: String, lang: String): Future<String> =
        selectAsLexicalForm(
            vocabulary, "?prefLabel",
            """
                <$concept> <${SKOS.prefLabel}> ?prefLabel
                FILTER(lang(?prefLabel)="$lang")
            """
        )

    fun getAltLabels(vocabulary: String, concept: String): Future<List<Pair<String, String>>> =
        selectAsLiteralPairList(vocabulary, "?altLabel", "<$concept> <${SKOS.altLabel}> ?altLabel")

    fun getAltLabel(vocabulary: String, concept: String, lang: String): Future<String> =
        selectAsLexicalForm(
            vocabulary, "?altLabel",
            """
                <$concept> <${SKOS.altLabel}> ?altLabel
                FILTER(lang(?altLabel)="$lang")
            """
        )

    fun getLabel(vocabulary: String, concept: String, lang: String): Future<String> =
        Promise.promise<String>().apply {
            getPrefLabel(vocabulary, concept, lang)
                .onSuccess { complete(it) }
                .onFailure {
                    getAltLabel(vocabulary, concept, lang)
                        .onSuccess { complete(it) }
                        .onFailure(::fail)
                }
        }.future()

    fun getConcepts(vocabulary: String): Future<List<Resource>> =
        selectAsResourceList(vocabulary, "?concept", "?concept <${RDF.type}> <${SKOS.Concept}>")

    fun isConcept(vocabulary: String, concept: String): Future<Boolean> =
        ask(vocabulary, "<$concept> <${RDF.type}> <${SKOS.Concept}>")

    fun getTableId(vocabulary: String, conceptScheme: String): Future<String> =
        selectAsLexicalForm(vocabulary, "?tableId", "<$conceptScheme> <${AT.table_id}> ?tableId")

    fun getNotationCode(vocabulary: String, concept: String, notationType: String): Future<String> =
        selectAsLexicalForm(
            vocabulary, "?notationCode",
            """
                <$concept> <${EUVOC.xlNotation}> ?xlNotation .
                ?xlNotation <${DCTerms.type}> <$notationType> ;
                            <${EUVOC.xlCodification}> ?notationCode
            """
        )

    fun getFromExactMatch(vocabulary: String, exactMatch: String): Future<Resource> =
        selectAsResource(
            vocabulary, "?fromExactMatch",
            "?fromExactMatch <${SKOS.exactMatch}> <$exactMatch>"
        )

    fun getFromPrefLabel(vocabulary: String, prefLabel: String, lang: String): Future<Resource> =
        selectAsResource(
            vocabulary, "?fromPrefLabel",
            "?fromPrefLabel <${SKOS.prefLabel}> \"$prefLabel\"@$lang"
        )

    fun getFromAltLabel(vocabulary: String, altLabel: String, lang: String): Future<Resource> =
        selectAsResource(
            vocabulary, "?fromAltLabel",
            "?fromAltLabel <${SKOS.altLabel}> \"$altLabel\"@$lang"
        )

    fun getCountryOfConcept(vocabulary: String, concept: String): Future<Resource> =
        selectAsResource(vocabulary, "?sfWithin", "<$concept> <${GEOSPARQL.sfWithin}> ?sfWithin")

    fun getLegacyCode(vocabulary: String, concept: String, sourceType: String): Future<String> =
        selectAsLexicalForm(
            vocabulary, "?legacyCode",
            """
           <$concept> <${AT.opMappedCode}> ?opMappedCode .
           ?opMappedCode <${DC_11.source}> "$sourceType" ;
                         <${AT.legacyCode}> ?legacyCode 
        """
        )

    fun isMachineReadable(vocabulary: String, concept: String): Future<Boolean> =
        ask(vocabulary, "<$concept> <${EDP.isMachineReadable}> true")

    fun isNonProprietary(vocabulary: String, concept: String): Future<Boolean> =
        ask(vocabulary, "<$concept> <${EDP.isNonProprietary}> true")

    fun getLicensingAssistant(vocabulary: String, concept: String): Future<Resource> =
        selectAsResource(
            vocabulary,
            "?licensingAssistant", "<$concept> <${EDP.licensingAssistant}> ?licensingAssistant"
        )

    fun isOpen(vocabulary: String, concept: String): Future<Boolean> =
        ask(vocabulary, "<$concept> <${OSI.isOpen}> true")

    fun hasConcepts(vocabulary: String): Future<Boolean> =
        ask(vocabulary, "?concept <${RDF.type}> <${SKOS.Concept}>")

    fun getConceptId(vocabulary: String, concept: String): Future<String> =
        Promise.promise<String>().apply {
            getIdentifier(vocabulary, concept).onSuccess { id ->
                complete(id)
            }.onFailure {
                complete(concept.substringAfterLast("/"))
            }
        }.future()

    fun getConceptSchemeId(vocabulary: String, conceptScheme: String): Future<String> =
        Promise.promise<String>().apply {
            getTableId(vocabulary, conceptScheme).onSuccess { id ->
                complete(id)
            }.onFailure {
                complete(conceptScheme.substringAfterLast("/"))
            }
        }.future()

    fun indexingSKOSVocabulary(vocabulary: String): Future<JsonObject> =
        Promise.promise<JsonObject>().apply {
            JsonObject().apply {
                put("resource", vocabulary)
                put("id", vocabulary.substringAfterLast("/"))
                JsonArray().also { concepts ->
                    put("vocab", concepts)
                    getConcepts(vocabulary).onSuccess { conceptsResult ->
                        val futures: MutableList<Future<Void>> = ArrayList()
                        conceptsResult.forEach { concept ->
                            val promise = Promise.promise<Void>()
                            futures.add(promise.future())
                            indexingSKOSVocable(vocabulary, concept.uri).onSuccess { conceptJson ->
                                if (conceptJson.containsKey("pref_label")) {
                                    concepts.add(conceptJson)
                                }
                                promise.complete()
                            }.onFailure { cause ->
                                promise.fail(cause)
                            }
                        }
                        CompositeFuture.all(futures.toList()).onSuccess {
                            complete(this)
                        }.onFailure(::fail)
                    }.onFailure(::fail)
                }
            }
        }.future()

    fun indexingSKOSVocable(vocabulary: String, concept: String): Future<JsonObject> =
        Promise.promise<JsonObject>().apply {
            JsonObject().apply {
                getConceptId(vocabulary, concept).onSuccess { id ->
                    put("resource", concept)
                    put("id", normalize(id))

                    val futures : MutableList<Future<Void>> = ArrayList()

                    val inSchemePromise = Promise.promise<Void>()
                    futures.add(inSchemePromise.future())
                    getConceptScheme(vocabulary, concept).onSuccess { conceptScheme ->
                        val inSchemeList = conceptScheme.asSequence().map { value -> value.uri }.toList()
                        if (inSchemeList.isNotEmpty()) {
                            put("in_scheme", conceptScheme.asSequence().map { value -> value.uri }.toList())
                        }
                        inSchemePromise.complete()
                    }.onFailure {
                        it.message?.let { message ->
                            if (message.contains("not found")) {
                                inSchemePromise.complete()
                            } else {
                                inSchemePromise.fail(it)
                            }
                        } ?: run {
                            inSchemePromise.fail(it)
                        }
                    }

                    val prefLabelPromise = Promise.promise<Void>()
                    futures.add(prefLabelPromise.future())
                    JsonObject().also { prefLabels ->
                        getPrefLabels(vocabulary, concept).onSuccess { prefLabelsResult ->
                            prefLabelsResult.forEach { (key, value) ->
                                // Only allow ISO 639-1 codes
                                prefLabels.putIfNotNull(
                                    key, when (key.length) {
                                        2 -> value
                                        else -> null
                                    }
                                )
                            }
                            putIfNotEmpty("pref_label", prefLabels)
                            prefLabelPromise.complete()
                        }.onFailure {
                            it.message?.let { message ->
                                if (message.contains("not found")) {
                                    prefLabelPromise.complete()
                                } else {
                                    prefLabelPromise.fail(it)
                                }
                            } ?: run {
                                prefLabelPromise.fail(it)
                            }
                        }
                    }

                    val altLabelPromise = Promise.promise<Void>()
                    futures.add(altLabelPromise.future())
                    JsonObject().also { altLabels ->
                        getAltLabels(vocabulary, concept).onSuccess { altLabelsResult ->
                            altLabelsResult.forEach { (key, value) ->
                                // Only allow ISO 639-1 codes
                                altLabels.putIfNotNull(
                                    key, when (key.length) {
                                        2 -> value
                                        else -> null
                                    }
                                )
                            }
                            putIfNotEmpty("alt_label", altLabels)
                            altLabelPromise.complete()
                        }.onFailure {
                            it.message?.let { message ->
                                if (message.contains("not found")) {
                                    altLabelPromise.complete()
                                } else {
                                    altLabelPromise.fail(it)
                                }
                            } ?: run {
                                prefLabelPromise.fail(it)
                            }
                        }
                    }

                    CompositeFuture.all(futures.toList()).onSuccess {
                        complete(this)
                    }.onFailure(::fail)
                }.onFailure(::fail)
            }
        }.future()

    private fun ask(vocabulary: String, where: String): Future<Boolean> =
        Promise.promise<Boolean>().apply {
            val query = buildAskQuery(vocabulary, where)

            tripleStore.ask(query).onSuccess {
                complete(it)
            }.onFailure(::fail)
        }.future()
    
    private fun selectAsResource(vocabulary: String, select: String, where: String): Future<Resource> =
        Promise.promise<Resource>().apply {
            val query = buildSelectQuery(vocabulary, select, where)

            tripleStore.select(query).onSuccess {
                if (it.hasNext()) {
                    it.next().run {
                        complete(getResource(select))
                    }
                } else {
                    fail("not found: $query")
                }
            }.onFailure(::fail)
        }.future()

    private fun selectAsResourceList(vocabulary: String, select: String, where: String): Future<List<Resource>> =
        Promise.promise<List<Resource>>().apply {
            val query = buildSelectQuery(vocabulary, select, where)

            tripleStore.select(query).onSuccess {
                if (it.hasNext()) {
                    complete(it.asSequence().map { qs -> qs.getResource(select) }.toList())
                } else {
                    complete(listOf())
                }
            }.onFailure(::fail)
        }.future()

    private fun selectAsLiteral(vocabulary: String, select: String, where: String): Future<Literal> =
        Promise.promise<Literal>().apply {
            val query = buildSelectQuery(vocabulary, select, where)

            tripleStore.select(query).onSuccess {
                if (it.hasNext()) {
                    it.next().run {
                        complete(getLiteral(select))
                    }
                } else {
                    fail("not found: $query")
                }
            }.onFailure(::fail)
        }.future()

    private fun selectAsLexicalForm(vocabulary: String, select: String, where: String): Future<String> =
        Promise.promise<String>().apply {
            selectAsLiteral(vocabulary, select, where).onSuccess {
                complete(it.lexicalForm)
            }.onFailure(::fail)
        }.future()

    private fun selectAsLiteralPairList(vocabulary: String, select: String, where: String):
            Future<List<Pair<String, String>>> =
        Promise.promise<List<Pair<String, String>>>().apply {
            val query = buildSelectQuery(vocabulary, select, where)

            tripleStore.select(query).onSuccess {
                if (it.hasNext()) {
                    complete(it.asSequence().map { qs -> Pair<String, String>(qs.getLiteral(select).language,
                        qs.getLiteral(select).lexicalForm)
                    }.toList())
                } else {
                    fail("not found: $query")
                }
            }.onFailure(::fail)
        }.future()

    private fun buildAskQuery(vocabulary: String, where: String): String =
        """
            ASK
            FROM <$vocabulary>
            WHERE { 
                $where
            }
        """
    
    private fun buildSelectQuery(vocabulary: String, select: String, where: String): String =
        """
            SELECT $select
            FROM <$vocabulary>
            WHERE { 
                $where
            }
        """

}
