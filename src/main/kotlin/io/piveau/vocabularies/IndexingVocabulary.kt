package io.piveau.vocabularies

import io.piveau.json.putIfNotNull
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject

fun indexingSKOSVocabulary(scheme: ConceptScheme): JsonObject = JsonObject().apply {
    put("resource", scheme.uriRef)
    put("id", scheme.id)
    JsonArray().also { concepts ->
        put("vocab", concepts)
        scheme.concepts.forEach { concept -> concepts.add(indexingSKOSVocable(concept)) }
    }
}

fun indexingSKOSVocable(concept: Concept): JsonObject = JsonObject()
    .put("id", concept.identifier)
    .put("resource", concept.resource.uri)
    .put("pref_label", JsonObject().also {
        concept.prefLabels.forEach { (key, value) ->
            // Only allow ISO 639-1 codes
            it.putIfNotNull(key, when (key.length) {
                2 -> value
                else -> null
            })
        }
    })
