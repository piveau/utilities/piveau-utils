package io.piveau.vocabularies

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient
import org.apache.jena.rdf.model.Resource

var applicationRemote: String? = null
var audioRemote: String? = null
var fontRemote: String? = null
var imageRemote: String? = null
var messageRemote: String? = null
var modelRemote: String? = null
var multipartRemote: String? = null
var textRemote: String? = null
var videoRemote: String? = null

fun enableIanaRemotes() {
    val client = WebClient.create(Vertx.vertx())

    applicationRemote = "https://www.iana.org/assignments/media-types/application.csv"
    audioRemote = "https://www.iana.org/assignments/media-types/audio.csv"
    fontRemote = "https://www.iana.org/assignments/media-types/font.csv"
    imageRemote = "https://www.iana.org/assignments/media-types/image.csv"
    messageRemote = "https://www.iana.org/assignments/media-types/message.csv"
    modelRemote = "https://www.iana.org/assignments/media-types/model.csv"
    multipartRemote = "https://www.iana.org/assignments/media-types/multipart.csv"
    textRemote = "https://www.iana.org/assignments/media-types/text.csv"
    videoRemote = "https://www.iana.org/assignments/media-types/video.csv"
}


object IanaType {

    private val list = readCsvResource("IANA/application.csv") +
            readCsvResource("IANA/audio.csv") +
            readCsvResource("IANA/font.csv") +
            readCsvResource("IANA/image.csv") +
            readCsvResource("IANA/message.csv") +
            readCsvResource("IANA/model.csv") +
            readCsvResource("IANA/multipart.csv") +
            readCsvResource("IANA/text.csv") +
            readCsvResource("IANA/video.csv")

    fun isMediaType(resource: Resource): Boolean = isMediaType(getMediaType(resource))

    fun isMediaType(mediaType: String) = list.contains(mediaType)

    fun getMediaType(resource: Resource) = resource.uri
        .substringBefore("#Resource")
        .substringAfter("/media-types/")
}

fun readCsvResource(resource: String) = resource.loadResource()?.let { inputStream ->
    csvReader().open(inputStream) {
        readAllWithHeaderAsSequence().map { it["Template"] ?: "" }.toList()
    }
} ?: listOf()
