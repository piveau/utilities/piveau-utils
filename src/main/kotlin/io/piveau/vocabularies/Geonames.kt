package io.piveau.vocabularies

import io.piveau.vocabularies.vocabulary.GEONAMES
import io.vertx.core.Future
import io.vertx.core.http.HttpResponseExpectation
import io.vertx.ext.web.client.predicate.ResponsePredicate
import org.apache.jena.rdf.model.*
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFParser
import org.apache.jena.vocabulary.RDF

private val regex = Regex("^https:\\/\\/sws.geonames.org\\/([0-9]+)")

fun isGeoNameFeature(uri: String): Boolean = regex.matches(uri)

fun loadGeoNameFeature(uri: String): Future<GeoNameFeature> = GeoNameFeature.create(uri)

private fun createValidRdfUri(uri: String): String =
    regex.find(uri)?.value?.plus("/about.rdf")
        ?: throw IllegalArgumentException("Geonames URI could not be resolved to rdf!")

private fun fetchGeoNameRDF(url: String) = client.getAbs(url)
    .timeout(2000)
    .send()
    .expecting(HttpResponseExpectation.SC_OK)
    .map { it.bodyAsString() }
    .map {
        val model = ModelFactory.createDefaultModel()
        RDFParser
            .fromString(it, Lang.RDFXML)
            .parse(model)

        model.listResourcesWithProperty(RDF.type, GEONAMES.Feature).asSequence().firstOrNull()
    }

class GeoNameFeature private constructor(private val feature: Resource?) {

    fun getName(): String? = feature?.getProperty(GEONAMES.name)?.literal?.lexicalForm

    fun getOfficialName(lang: String = "en"): String? =
        feature?.getProperty(GEONAMES.officialName, lang)?.literal?.lexicalForm

    fun getCountryCode(): String? = feature?.getProperty(GEONAMES.countryCode)?.literal?.lexicalForm

    companion object {
        fun create(uri: String): Future<GeoNameFeature> {
            val validUri = createValidRdfUri(uri)
            return fetchGeoNameRDF(validUri).map { GeoNameFeature(it) }
        }
    }
}
