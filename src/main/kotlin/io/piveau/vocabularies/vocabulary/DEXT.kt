package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource

object DEXT {
    const val NS = "https://data.europa.eu/ns/ext#"

    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val MetadataExtension: Resource = m.createResource("${NS}MetadataExtension")

    @JvmField
    val metadataExtension = m.createProperty(NS, "metadataExtension")

    @JvmField
    val isUsedBy = m.createProperty(NS, "isUsedBy")

}
