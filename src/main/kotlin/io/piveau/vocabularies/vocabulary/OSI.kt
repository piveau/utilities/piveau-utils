package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property

object OSI {
    const val NS = "https://piveau.eu/ns/osi#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val isOpen: Property = m.createProperty(NS, "isOpen")
}
