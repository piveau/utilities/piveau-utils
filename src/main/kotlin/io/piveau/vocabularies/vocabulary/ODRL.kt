package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property

object ODRL {
    const val NS = "https://www.w3.org/TR/odrl-vocab/#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val hasPolicy: Property = m.createProperty(NS, "hasPolicy")
}
