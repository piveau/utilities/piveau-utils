package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property

object DCATAPDE {
    const val NS = "http://dcat-ap.de/def/dcatde/"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val legalBase: Property = m.createProperty(NS, "legalBase")

    @JvmField
    val geocodingDescription: Property = m.createProperty(NS, "geocodingDescription")

    @JvmField
    val contributorID: Property = m.createProperty(NS, "contributorID")

    @JvmField
    val qualityProcessURI: Property = m.createProperty(NS, "qualityProcessURI")

    @JvmField
    val originator: Property = m.createProperty(NS, "originator")

    @JvmField
    val maintainer: Property = m.createProperty(NS, "maintainer")

    @JvmField
    val politicalGeocodingLevelURI: Property = m.createProperty(NS, "politicalGeocodingLevelURI")

    @JvmField
    val politicalGeocodingURI: Property = m.createProperty(NS, "politicalGeocodingURI")

    @JvmField
    val plannedAvailability: Property = m.createProperty(NS, "plannedAvailability")

    @JvmField
    val licenseAttributionByText: Property = m.createProperty(NS, "licenseAttributionByText")

}
