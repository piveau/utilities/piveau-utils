package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property

object TIME {
    const val NS = "http://www.w3.org/2006/time#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val hasBeginning: Property = m.createProperty(NS, "hasBeginning")
    @JvmField
    val hasEnd: Property = m.createProperty(NS, "hasEnd")
}
