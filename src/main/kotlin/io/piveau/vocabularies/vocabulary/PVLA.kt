package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object PVLA {
    const val NS = "https://piveau.eu/ns/licensing-assistant#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val addendum: Property = m.createProperty(NS, "addendum")

    @JvmField
    val Addendum: Resource = m.createResource("${NS}Addendum")

    @JvmField
    val permissions: Property = m.createProperty(NS, "permissions")

    @JvmField
    val Permissions: Resource = m.createResource("${NS}Permissions")

    @JvmField
    val reproduction: Property = m.createProperty(NS, "reproduction")

    @JvmField
    val distribution: Property = m.createProperty(NS, "distribution")

    @JvmField
    val derivativeWorks: Property = m.createProperty(NS, "derivativeWorks")

    @JvmField
    val sublicensing: Property = m.createProperty(NS, "sublicensing")

    @JvmField
    val patentGrant: Property = m.createProperty(NS, "patentGrant")

    @JvmField
    val requirements: Property = m.createProperty(NS, "requirements")

    @JvmField
    val Requirements: Resource = m.createResource("${NS}Requirements")

    @JvmField
    val notice: Property = m.createProperty(NS, "notice")

    @JvmField
    val attribution: Property = m.createProperty(NS, "attribution")

    @JvmField
    val shareAlike: Property = m.createProperty(NS, "shareAlike")

    @JvmField
    val copyleft: Property = m.createProperty(NS, "copyleft")

    @JvmField
    val lesserCopyleft: Property = m.createProperty(NS, "lesserCopyleft")

    @JvmField
    val stateChanges: Property = m.createProperty(NS, "stateChanges")

    @JvmField
    val prohibitions: Property = m.createProperty(NS, "prohibitions")

    @JvmField
    val Prohibitions: Resource = m.createResource("${NS}Prohibitions")

    @JvmField
    val commercial: Property = m.createProperty(NS, "commercial")

    @JvmField
    val useTrademark: Property = m.createProperty(NS, "useTrademark")

    @JvmField
    val databaseRightsCovered: Property = m.createProperty(NS, "databaseRightsCovered")

}