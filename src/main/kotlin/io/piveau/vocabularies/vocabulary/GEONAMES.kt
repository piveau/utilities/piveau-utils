package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object GEONAMES {
    const val NS = "http://www.geonames.org/ontology#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val name = m.createProperty("http://www.geonames.org/ontology#name")

    @JvmField
    val officialName = m.createProperty("http://www.geonames.org/ontology#officialName")

    @JvmField
    val alternateName = m.createProperty("http://www.geonames.org/ontology#alternateName")

    @JvmField
    val countryCode = m.createProperty("http://www.geonames.org/ontology#countryCode")

    @JvmField
    val Feature = m.createProperty("http://www.geonames.org/ontology#Feature")

}
