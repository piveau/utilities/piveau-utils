package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object DCATAP_SHACL_SHAPES {
    const val NS = "http://data.europa.eu/r5r#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val Agent_Shape: Resource = m.createResource("${NS}Agent_Shape")
    @JvmField
    val DateOrDateTimeDataType_Shape: Resource = m.createResource("${NS}DateOrDateTimeDataType_Shape")
    @JvmField
    val Catalog_Shape: Resource = m.createResource("${NS}Catalog_Shape")
    @JvmField
    val CatalogRecord_Shape: Resource = m.createResource("${NS}CatalogRecord_Shape")
    @JvmField
    val Dataset_Shape: Resource = m.createResource("${NS}Dataset_Shape")
    @JvmField
    val Distribution_Shape: Resource = m.createResource("${NS}Distribution_Shape")
    @JvmField
    val CategoryScheme_Shape: Resource = m.createResource("${NS}CategoryScheme_Shape")
    @JvmField
    val Category_Shape: Resource = m.createResource("${NS}Category_Shape")
    @JvmField
    val Checksum_Shape: Resource = m.createResource("${NS}Checksum_Shape")
    @JvmField
    val Identifier_Shape: Resource = m.createResource("${NS}Identifier_Shape")
    @JvmField
    val LicenceDocument_Shape: Resource = m.createResource("${NS}LicenceDocument_Shape")
    @JvmField
    val PeriodOfTime_Shape: Resource = m.createResource("${NS}PeriodOfTime_Shape")

}
