package io.piveau.utils

import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

/**
 * Normalize date time strings to java [Instant] standard format and date strings to yyy-MM-dd
 *
 * @param dateTime Date or date time [String]
 * @return Normalized [String] or null
 */
fun normalizeDateTime(dateTime: String): String? = parseZonedDateTime(dateTime)

private fun parseZonedDateTime(dateTime: String): String? = try {
    ZonedDateTime.parse(dateTime).toInstant().toString()
} catch (e: DateTimeParseException) {
    parseLocalDateTime(dateTime)
}

private fun parseLocalDateTime(dateTime: String): String? = try {
    LocalDateTime.parse(dateTime).atZone(ZoneId.systemDefault()).toInstant().toString()
} catch (e: DateTimeParseException) {
    parseLocalDate(dateTime)
}

private fun parseLocalDate(date: String): String? = try {
    LocalDate.parse(date).toString()
} catch (e: DateTimeParseException) {
    parseDate(date)
}

private fun parseDate(date: String): String? = try {
    LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy/MM/dd")).toString()
} catch (e: DateTimeParseException) {
    null
}
