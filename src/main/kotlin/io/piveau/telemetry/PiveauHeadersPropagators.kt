package io.piveau.telemetry

import io.opentelemetry.context.propagation.TextMapGetter
import io.opentelemetry.context.propagation.TextMapSetter
import io.vertx.core.http.HttpClientRequest
import io.vertx.core.http.HttpServerRequest

class PiveauHeadersPropagatorGetter : TextMapGetter<HttpServerRequest> {
    override fun keys(request: HttpServerRequest): MutableIterable<String> {
        return request.headers().map { it.key }.toMutableList()
    }

    override fun get(request: HttpServerRequest?, key: String): String? {
        return request?.getHeader(key)
    }
}

class PiveauHeadersPropagatorSetter : TextMapSetter<HttpClientRequest> {
    override fun set(request: HttpClientRequest?, key: String, value: String) {
        request?.putHeader(key, value)
    }
}
