package io.piveau.telemetry

import io.opentelemetry.api.OpenTelemetry
import io.opentelemetry.api.trace.Span
import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.context.Context
import io.opentelemetry.sdk.autoconfigure.AutoConfiguredOpenTelemetrySdk
import io.piveau.pipe.model.Pipe
import io.vertx.core.json.JsonObject

object PiveauTelemetry {

    private val openTelemetry = AutoConfiguredOpenTelemetrySdk.initialize().openTelemetrySdk

    @JvmStatic
    fun init(): OpenTelemetry {
        return openTelemetry
    }

    @JvmStatic
    fun getTracer(name: String) = PiveauTelemetryTracer(name, openTelemetry)
}

class PiveauTelemetryTracer(name: String, private val openTelemetry: OpenTelemetry) {
    private val tracer = openTelemetry.getTracer(name)

    @JvmOverloads
    fun childSpan(spanName: String, spanKind: SpanKind = SpanKind.SERVER): Span {
        val spanBuilder = tracer
            .spanBuilder(spanName)
            .setSpanKind(spanKind)

        Span.fromContextOrNull(Context.current())?.let {
            spanBuilder.setParent(Context.current())
        }

        return spanBuilder.startSpan()
    }

    @JvmOverloads
    fun span(spanName: String, spanKind: SpanKind = SpanKind.INTERNAL): Span {
        return Span.fromContextOrNull(Context.current())?.updateName(spanName) ?: tracer
            .spanBuilder(spanName)
            .setSpanKind(spanKind)
            .startSpan()
    }

    @JvmOverloads
    fun newSpan(spanName: String, spanKind: SpanKind = SpanKind.INTERNAL): Span {
        return tracer
            .spanBuilder(spanName)
            .setSpanKind(spanKind)
            .startSpan()
    }

    suspend fun span(name: String, pipe: Pipe, kind: SpanKind = SpanKind.INTERNAL, spanBody: suspend Span.() -> Unit ) {
        val span = span(name, kind)
        span.setPipeAttributes(pipe)
        val scope = span.makeCurrent()
        span.spanBody()
        scope.close()
        span.end()
    }

    suspend fun span(name: String, info: JsonObject = JsonObject(), kind: SpanKind = SpanKind.INTERNAL, spanBody: suspend Span.() -> Unit ) {
        val span = span(name, kind)
        span.setPipeResourceAttributes(info)
        val scope = span.makeCurrent()
        span.spanBody()
        scope.close()
        span.end()
    }

}
