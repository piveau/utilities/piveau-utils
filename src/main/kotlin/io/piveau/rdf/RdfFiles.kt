package io.piveau.rdf

import io.piveau.dcatap.setNsPrefixesFiltered
import io.vertx.core.Future
import io.vertx.core.Vertx
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFLanguages
import org.apache.jena.riot.RDFParser
import org.apache.jena.riot.RDFWriter
import java.io.File
import java.nio.file.Path

fun File.loadToModel(vertx: Vertx): Future<Model> = toPath().loadToModel(vertx)

fun Path.loadToModel(vertx: Vertx): Future<Model> = vertx.fileSystem()
    .readFile(this.toString())
    .map {
        ModelFactory.createDefaultModel().apply {
            RDFParser.create()
                .checking(false)
                .lang(RDFLanguages.filenameToLang(this@loadToModel.fileName.toString()))
                .source(it.bytes.inputStream())
                .parse(this)
        }
    }

fun Model.saveToFile(filename: String, lang: Lang, vertx: Vertx): Future<Void> = vertx.executeBlocking<Void> { promise ->
    setNsPrefixesFiltered()
    RDFWriter.create()
        .source(this)
        .lang(lang)
        .context(cxt)
        .output(filename)

    promise.complete()
}

fun Model.saveToTmpFile(lang: Lang, vertx: Vertx): Future<String> = vertx.fileSystem()
    .createTempFile("piveau", "-tmp.${lang.fileExtensions[0]}")
    .compose {
        saveToFile(it, lang, vertx).map(it)
    }
