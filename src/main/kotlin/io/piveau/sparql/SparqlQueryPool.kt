package io.piveau.sparql

import io.vertx.core.Vertx
import java.nio.file.Path

class SparqlQueryPool private constructor(private val queries: Map<String, String> = mapOf()) {

    companion object {
        fun create(vertx: Vertx, path: Path): SparqlQueryPool {
            val queries = vertx.fileSystem().readDirBlocking(path.toString(), "([a-zA-Z0-9\\s_\\\\.\\-\\(\\):])+(.sparql)$")
            val map = queries.associate { file ->
                val query = vertx.fileSystem().readFileBlocking(file).toString()
                val name = Path.of(file).fileName.toString().removeSuffix(".sparql")
                name to query
            }

            return SparqlQueryPool(map)
        }
    }

    fun format(id: String, vararg args: String) = queries[id]?.let { String.format(it.straighten(), *args) }

    fun raw(id: String) = queries[id]

    fun straighten(id: String) = queries[id]?.straighten()

    fun list() = queries.keys
}

private fun String.straighten() = replace("\\s+".toRegex(), " ")