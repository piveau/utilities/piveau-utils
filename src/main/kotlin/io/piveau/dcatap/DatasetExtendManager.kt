package io.piveau.dcatap

import io.piveau.vocabularies.vocabulary.EDP
import io.vertx.core.Future
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicReference


enum class FilterType {
    INCLUDE,

    EXCLUDE,
    ONLY;

}

class DatasetExtendManager internal constructor(
    private val tripleStore: TripleStore,
    private val datasetManager: DatasetManager
) {

    private val log = LoggerFactory.getLogger(javaClass)

    fun exist(uriRef: DCATAPUriRef) =
        tripleStore.ask("ASK WHERE { GRAPH <${uriRef.graphNameRef}> { <${uriRef.uriRef}> a <${DCAT.Dataset}> } }")

    fun existGraph(graphName: URIRef): Future<Boolean> =
        tripleStore.ask("ASK WHERE { GRAPH <$graphName> { ?s ?p ?o } }")

    @JvmOverloads
    fun list(
        offset: Int = 0,
        limit: Int = 100,
        restricted: FilterType = FilterType.INCLUDE,
        drafts: FilterType = FilterType.EXCLUDE
    ): Future<List<DCATAPUriRef>> =
        tripleStore.select("SELECT ?dataset WHERE { GRAPH ?catalogue { ?cat a <${DCAT.Catalog}> ; <${DCAT.dataset}> ?dataset FILTER NOT EXISTS { ?cat <${EDP.visibility}> <${EDP.hidden}> } } } OFFSET $offset LIMIT $limit")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .toList()
            }


    @JvmOverloads
    fun count(
        restricted: FilterType = FilterType.INCLUDE,
        drafts: FilterType = FilterType.EXCLUDE
    ): Future<Int> =
        tripleStore.select("SELECT (count(?dataset) AS ?count) WHERE { GRAPH ?catalogue { ?cat a <${DCAT.Catalog}> ; <${DCAT.dataset}> ?dataset FILTER NOT EXISTS { ?cat <${EDP.visibility}> <${EDP.hidden}> } } }")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> qs.getLiteral("count").int }
                    .toList().first()
            }

    @JvmOverloads
    fun listModels(
        offset: Int = 0,
        limit: Int = 100,
        restricted: FilterType = FilterType.INCLUDE,
        drafts: FilterType = FilterType.EXCLUDE
    ): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()
        return tripleStore.select("SELECT ?dataset WHERE { GRAPH ?catalogue { ?cat a <${DCAT.Catalog}> ; <${DCAT.dataset}> ?dataset FILTER NOT EXISTS { ?cat <${EDP.visibility}> <${EDP.hidden}> } } } OFFSET $offset LIMIT $limit")
            .compose { result ->
                val futures = result.asSequence()
                    .map { DCATAPUriSchema.parseUriRef(it.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .map { getGraph(it.uriRef) }
                    .toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map { it.list<Model>().filterNotNull() }
            .otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun identify(originId: String, catalogueId: String): Future<Pair<Resource, Resource>> =
        datasetManager.identify(originId, catalogueId)

    fun catalogue(datasetUriRef: URIRef): Future<Resource> = datasetManager.catalogue(datasetUriRef)


    fun originIdentifier(datasetUriRef: URIRef): Future<String> = datasetManager.originIdentifier(datasetUriRef)

    fun distributions(datasetUriRef: URIRef): Future<List<DCATAPUriRef>> = datasetManager.distributions(datasetUriRef)

    fun identifyDistribution(distributionUriRef: URIRef): Future<DCATAPUriRef> =
        datasetManager.identifyDistribution(distributionUriRef)

    fun delete(originId: String, catalogueId: String): Future<Unit> =
        datasetManager.delete(originId, catalogueId)

    fun deleteGraph(name: URIRef) = datasetManager.deleteGraph(name)

    fun get(originId: String, catalogueId: String): Future<Model> = datasetManager.get(originId, catalogueId)

    fun getGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o } }")

    fun set(originId: String, catalogueId: String, model: Model): Future<String> =
        Future.failedFuture(NotImplementedError())

    fun setGraph(name: URIRef, model: Model, clearGeoData: Boolean): Future<String> =
        tripleStore.setGraph(name, model, clearGeoData)


}