@file:JvmName(name = "Prefixes")
package io.piveau.dcatap

import io.piveau.vocabularies.vocabulary.*
import org.apache.jena.query.Dataset
import org.apache.jena.rdf.model.*
import org.apache.jena.shared.PrefixMapping
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.*

@JvmField
val DCATAP_PREFIXES = mapOf(
    "dcat" to DCAT.NS,
    "skos" to SKOS.uri,
    "skosxl" to SKOSXL.uri,
    "foaf" to FOAF.NS,
    "dct" to DCTerms.NS,
    "gsp" to "http://www.opengis.net/ont/geosparql#",
    "odrl" to "https://www.w3.org/TR/odrl-vocab/#",
    "dcatap" to DCATAP.NS,
    "prov" to PROV.NS,
    "owl" to OWL.NS,
    "xsd" to XSD.NS,
    "rdfs" to RDFS.uri,
    "gmd" to "http://www.isotc211.org/2005/gmd#",
    "vcard" to VCARD4.NS,
    "adms" to ADMS.NS,
    "spdx" to SPDX.NS,
    "schema" to "https://schema.org/",
    "locn" to LOCN.NS,
    "org" to ORG.NS,
    "time" to "http://www.w3.org/2006/time#",
    "hydra" to HYDRA.NS,
    "edp" to EDP.NS,
    "dcatapde" to DCATAPDE.NS,
    "euvoc" to EUVOC.NS,
    "dqv" to DQV.NS,
    "lemon" to "http://lemon-model.net/lemon#",
    "at" to AT.NS,
    "oa" to "http://www.w3.org/ns/oa#",
    "shacl" to SHACL.NS,
    "sis" to "http://senias.de/ns/sis#",
    "stat" to STATDCATAP.NS,
    "osi" to OSI.NS,
    "dext" to DEXT.NS,
    "gx" to "https://w3id.org/gaia-x/development#",
    "rdf" to "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "xsd" to "http://www.w3.org/2001/XMLSchema#",
    "piveau" to "https://piveau.eu/ns/voc#",
    "vcard" to "http://www.w3.org/2006/vcard/ns#"
)


@JvmField
val DCATAP_PREFIXES_REVERSED = DCATAP_PREFIXES.entries.associate{(k,v) -> v to k}

fun Model.setNsPrefixesFiltered() {
    clearNsPrefixMap()
    setPrefixes(listNameSpaces(), this)
}

fun Dataset.setNsPrefixesFiltered() {
    prefixMapping.clearNsPrefixMap()
    setPrefixes(defaultModel.listNameSpaces(), prefixMapping)
    setPrefixes(unionModel.listNameSpaces(), prefixMapping)
}

internal fun setPrefixes(iterator: NsIterator, prefixMapping: PrefixMapping) {
    iterator.forEach { namespace ->
        DCATAP_PREFIXES_REVERSED[namespace]?.let {
            prefixMapping.setNsPrefix(it, namespace)
        }
    }
}
