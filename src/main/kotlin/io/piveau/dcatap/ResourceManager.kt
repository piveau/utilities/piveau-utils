package io.piveau.dcatap

import io.piveau.vocabularies.vocabulary.EDP
import io.piveau.vocabularies.vocabulary.PV
import io.vertx.core.Future
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import java.util.concurrent.atomic.AtomicReference

class ResourceManager internal constructor(private val tripleStore: TripleStore) {


    @JvmOverloads
    fun list(typeUri: String, inCatalog: Boolean = true, offset: Int = 0, limit: Int = 100): Future<List<String>> {
        val query = if (inCatalog) {
            """
                SELECT ?resource WHERE 
                { 
	                GRAPH ?catalogue 
	                { 
		                ?cat a <${DCAT.Catalog}> ; 
		                <${PV.resource}> ?resource 
		                FILTER NOT EXISTS 
		                { 
			                ?cat <${EDP.visibility}> <${EDP.hidden}> 
		                } 
	                } 
	
	                GRAPH ?resource
                    {
                        ?resource a <$typeUri>
                    }
                } OFFSET $offset LIMIT $limit
            """

        } else {
            "SELECT ?resource WHERE { ?resource a <$typeUri> } OFFSET $offset LIMIT $limit"
        }


        return tripleStore.select(query)
            .map { resultSet ->
                resultSet.asSequence().map { qs -> qs.getResource("resource").uri }
                    .toList()
            }
    }


    fun count(typeUri: String, inCatalog: Boolean = true): Future<Int> {
        val query = if (inCatalog) {
            """
                SELECT (count(?resource) AS ?count) WHERE 
                { 
	                GRAPH ?catalogue 
	                { 
		                ?cat a <${DCAT.Catalog}> ; 
		                <${PV.resource}> ?resource 
		                FILTER NOT EXISTS 
		                { 
			                ?cat <${EDP.visibility}> <${EDP.hidden}> 
		                } 
	                } 
	
	                GRAPH ?resource
                    {
                        ?resource a <$typeUri>
                    }
                } 
            """

        } else {
            "SELECT (count(?resource) AS ?count) WHERE { ?resource a <$typeUri> }"
        }



        return tripleStore.select(query)
            .map { resultSet ->
                resultSet.asSequence().map { qs -> qs.getLiteral("count").int }
                    .toList().first()
            }


    }


    @JvmOverloads
    fun listModels(typeUri: String, inCatalog: Boolean = true,offset: Int = 0, limit: Int = 100): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()

        return list(typeUri, inCatalog, offset, limit)
            .compose { uris ->
                val futures = uris.map { getGraph(it) }
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map { it.list<Model>().filterNotNull() }
            .otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun identify(resourceId: String, catalogueId: String): Future<Pair<Resource, Resource>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query = """
                SELECT ?resource ?record
                WHERE
                {
                    GRAPH <${catalogueUriRef.graphNameRef}> 
                    {
                        <${catalogueUriRef.uriRef}> <${PV.resource}> ?resource 
                    } 
                    
                    GRAPH ?resource
                    {
                        ?record <${DCTerms.identifier}> "$resourceId" ; 
                                <${FOAF.primaryTopic}> ?resource 
                    }
                }
                """

        return tripleStore.select(query)
            .map {
                if (it.hasNext()) {
                    it.next().run {
                        Pair<Resource, Resource>(getResource("resource"), getResource("record"))
                    }
                } else {
                    throw TripleStoreException(404, DATASET_NOT_FOUND)
                }
            }
    }

    fun catalog(resourceUriRef: URIRef): Future<Resource> {
        val query = "SELECT ?catalogue WHERE { GRAPH ?catalogue { ?catalogue <${PV.resource}> <$resourceUriRef> } }"

        return tripleStore.select(query)
            .map {
                if (it.hasNext()) {
                    it.next().getResource("catalogue")
                } else {
                    throw TripleStoreException(404, CATALOGUE_NOT_FOUND)
                }
            }
    }


    fun get(originId: String, catalogueId: String): Future<Model> =
        identify(originId, catalogueId).compose {
            getGraph(it.first.uri)
        }


    fun getGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o } }")


}