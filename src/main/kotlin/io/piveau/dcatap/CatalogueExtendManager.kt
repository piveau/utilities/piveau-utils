package io.piveau.dcatap

import io.piveau.security.PermissionManager.ACCESS_LEVEL
import io.piveau.vocabularies.vocabulary.PV
import io.vertx.core.Future
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.flow.*
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicReference
import kotlin.sequences.filter
import kotlin.sequences.map
import kotlin.sequences.toList

class CatalogueExtendManager internal constructor(
    private val tripleStore: TripleStore,
    private val catalogueManager: CatalogueManager
) {

    val log: Logger = LoggerFactory.getLogger(javaClass)

    fun exist(catalogueId: String) = catalogueManager.exist(catalogueId)

    fun ensureExists(catalogueId: String): Future<Unit> = catalogueManager.ensureExists(catalogueId)

    fun isProtected(catalogueId: String): Future<Boolean> = DCATAPUriSchema.createForCatalogue(catalogueId).let {
        tripleStore.ask("ASK WHERE { GRAPH <${it.graphNameRef}> { <${it.graphNameRef}> <${PV.visibilityPublic}>  \"false\"^^<http://www.w3.org/2001/XMLSchema#boolean>.} }")
    }

    fun listUris(
        restricted: FilterType = FilterType.INCLUDE,
        allowed: List<String> = listOf()
    ): Future<List<Pair<DCATAPUriRef, ACCESS_LEVEL>>> {

        val restrictedQuery = when (restricted) {
            FilterType.INCLUDE -> "OPTIONAL { ?g <${PV.visibilityPublic}> ?r. }"
            FilterType.EXCLUDE -> " ?g <${PV.visibilityPublic}> \"true\"^^<http://www.w3.org/2001/XMLSchema#boolean>. "
            FilterType.ONLY -> " ?g <${PV.visibilityPublic}> \"false\"^^<http://www.w3.org/2001/XMLSchema#boolean>. "
        }

        if (restricted == FilterType.INCLUDE) {
            return tripleStore.select("SELECT DISTINCT ?g ?r WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}>. $restrictedQuery } }")
                .map { resultSet ->
                    resultSet.asSequence().map {
                        if (it.get("r") == null || !it.get("r").isLiteral) {

                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                ACCESS_LEVEL.PUBLIC
                            )
                        } else {
                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                if (it.getLiteral("r").boolean) ACCESS_LEVEL.PUBLIC else ACCESS_LEVEL.RESTRICTED
                            )
                        }
                    }
                        .filter { it.first.isValid() }
                        .filter { it.second == ACCESS_LEVEL.PUBLIC || allowed.contains(it.first.id) }
                        .toList()
                }
        } else {
            return tripleStore.select("SELECT DISTINCT ?g WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}>. $restrictedQuery } }")
                .map { resultSet ->
                    resultSet.asSequence().map {
                        Pair(
                            DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                            if (restricted == FilterType.EXCLUDE) ACCESS_LEVEL.PUBLIC else ACCESS_LEVEL.RESTRICTED
                        )
                    }
                        .filter { it.first.isValid() }
                        .filter { it.second == ACCESS_LEVEL.PUBLIC || allowed.contains(it.first.id) }
                        .toList()
                }
        }

    }

    @JvmOverloads
    fun list(
        offset: Int = 0, limit: Int = 100,
        restricted: FilterType = FilterType.INCLUDE,
        allowed: List<String> = listOf()
    ): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()


        val restrictedQuery = when (restricted) {
            FilterType.INCLUDE -> "OPTIONAL { ?g <${PV.accessLevel}> ?r. }"
            FilterType.EXCLUDE -> " ?g <${PV.accessLevel}> <${PV.visibilityPublic}>. "
            FilterType.ONLY -> " ?g <${PV.accessLevel}> <${PV.visibilityRestricted}>. "
        }

        // Take care. If more than max order by limit catalogues exists, this query will not work (usually 10.000)
        return tripleStore.select("SELECT DISTINCT ?g WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}>. $restrictedQuery } } ORDER BY ?g OFFSET $offset LIMIT $limit")
            .compose { result ->
                val futures = result.asSequence()
                    .map {
                        if (it.get("r") == null || !it.get("r").isLiteral) {

                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                if (restricted == FilterType.ONLY) ACCESS_LEVEL.RESTRICTED else ACCESS_LEVEL.PUBLIC
                            )
                        } else {
                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                if (it.getLiteral("r").boolean) ACCESS_LEVEL.PUBLIC else ACCESS_LEVEL.RESTRICTED
                            )
                        }
                    }
                    .filter { it.first.isValid() }
                    .filter { it.second == ACCESS_LEVEL.PUBLIC || allowed.contains(it.first.id) }
                    .map { getStrippedGraph(it.first.uriRef) }
                    .toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map { it.list<Model>().filterNotNull() }
            .otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun subCatalogues(
        catalogueId: String,
        restricted: FilterType = FilterType.INCLUDE
    ): Future<List<DCATAPUriRef>> {
        val schema = DCATAPUriSchema.createForCatalogue(catalogueId)

        val restrictedQuery = when (restricted) {
            FilterType.INCLUDE -> ""
            FilterType.EXCLUDE -> " GRAPH ?sc { ?sc  <${PV.accessLevel}> <${PV.visibilityPublic}>. }  "
            FilterType.ONLY -> " GRAPH ?sc { ?sc  <${PV.accessLevel}> <${PV.visibilityRestricted}>. } "
        }

        return tripleStore.select("SELECT ?sc WHERE { GRAPH <${schema.graphNameRef}> { <${schema.uriRef}> <${DCTerms.hasPart}> ?sc } $restrictedQuery")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("sc").uri) }
                    .filter { it.isValid() }
                    .toList()
            }
    }

    fun allRecords(
        catalogueId: String,
        restricted: FilterType = FilterType.INCLUDE,
        drafts: FilterType = FilterType.EXCLUDE,
        allowed: List<String> = listOf()
    ): Future<List<DCATAPUriRef>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query =
            "SELECT ?r WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.record}> ?r } }"

        return tripleStore.recursiveSelect(query, 0, 5000, mutableListOf())
            .map { list ->
                list.flatMap { it.asSequence() }
                    .map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("r").uri) }
                    .filter { it.isValid() }
            }
    }

    @JvmOverloads
    fun records(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<DCATAPUriRef>> =
        catalogueManager.records(catalogueId, offset, limit)

    fun allDatasets(catalogueId: String): Future<List<DCATAPUriRef>> = catalogueManager.allDatasets(catalogueId)

    fun countDatasets(catalogueId: String): Future<Int> = catalogueManager.countDatasets(catalogueId)

    @JvmOverloads
    fun datasets(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<DCATAPUriRef>> =
        catalogueManager.datasets(catalogueId, offset, limit)


    // TODO: allow a list of visibilities and null -> all
    @JvmOverloads
    fun datasets(catalogueId: String, offset: Int = 0, limit: Int = 100, visibility: Resource): Future<List<DCATAPUriRef>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query = "SELECT DISTINCT ?dataset WHERE\n" +
                "            {\n" +
                "            GRAPH <${catalogueUriRef.graphNameRef}> {\n" +
                "                <${catalogueUriRef.graphNameRef}> <${DCAT.record}> ?record\n" +
                "            }\n" +
                "\n" +
                "            GRAPH ?dataset {\n" +
                "                ?record <${PV.visibility}> ?visibility.\n" +
                "                FILTER (?visibility = <${visibility}>)\n" +
                "            }\n" +
                "        } OFFSET $offset LIMIT $limit "

        return tripleStore.select(query)
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .toList()
            }
    }

    @JvmOverloads
    fun listDatasets(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<Model>> =
        catalogueManager.listDatasets(catalogueId, offset, limit)

    @JvmOverloads
    fun listDatasets(catalogueId: String, offset: Int = 0, limit: Int = 100, visibility: Resource): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        // TODO: extract query because it is equal to the query that is used in dataset method
        val query = "SELECT DISTINCT ?dataset WHERE\n" +
                "            {\n" +
                "            GRAPH <${catalogueUriRef.graphNameRef}> {\n" +
                "                <${catalogueUriRef.graphNameRef}> <${DCAT.record}> ?record\n" +
                "            }\n" +
                "\n" +
                "            GRAPH ?dataset {\n" +
                "                ?record <${PV.visibility}> ?visibility.\n" +
                "                FILTER (?visibility = <${visibility}>)\n" +
                "            }\n" +
                "        } OFFSET $offset LIMIT $limit "



        return tripleStore.select(query)
            .compose { result ->
                val futures = result.asSequence()
                    .map { DCATAPUriSchema.parseUriRef(it.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .map { getGraph(it.uriRef) }.toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map {
                it.list<Model>().filterNotNull()
            }.otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }


    fun addDatasetEntry(
        catalogueGraphName: String,
        catalogueUriRef: String,
        datasetUriRef: String,
        recordUriRef: String
    ): Future<Unit> {
        return tripleStore.update(
            "INSERT DATA { GRAPH <$catalogueGraphName> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <$recordUriRef> ; <http://www.w3.org/ns/dcat#dataset> <$datasetUriRef> . } }"
        )
    }

    fun removeDatasetEntry(catalogueUriRef: URIRef, datasetUriRef: URIRef): Future<Unit> {
        val catalogueDCATAPUriRef = DCATAPUriSchema.parseUriRef(catalogueUriRef)
        val datasetDCATAPUriRef = DCATAPUriSchema.parseUriRef(datasetUriRef)
        return tripleStore.update(
            "DELETE DATA { GRAPH <${catalogueDCATAPUriRef.graphNameRef}> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <${datasetDCATAPUriRef.recordUriRef}> ; <http://www.w3.org/ns/dcat#dataset> <$datasetUriRef> . } }"
        )
    }

    fun allDatasetIdentifiers(catalogueId: String): Future<List<String>> =
        catalogueManager.allDatasetIdentifiers(catalogueId)

    //TODO: allow a list of visibilities and null -> all
    fun allDatasetIdentifiers(catalogueId: String, visibility: Resource): Future<List<String>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query =
            "SELECT DISTINCT ?identifier WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.record}> ?record } GRAPH ?d { ?record <${DCTerms.identifier}> ?identifier. " +
                    "?record <${PV.visibility}> ?visibility. " +
                    "FILTER (?visibility = <${visibility}>) } }"

        return tripleStore.recursiveSelect(query, 0, 5000, mutableListOf())
            .map { list -> list.flatMap { it.asSequence() }.map { qs -> qs.getLiteral("identifier").lexicalForm } }
    }

    fun datasetIdentifiers(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<String>> =
        catalogueManager.datasetIdentifiers(catalogueId, offset, limit)

    fun delete(catalogueId: String) = deleteGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun deleteGraph(name: URIRef) = tripleStore.deleteGraph(name)

    fun get(catalogueId: String) = getGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getRecordsStripped(catalogueId: String): Future<Model> =
        getStrippedGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getRecordsStrippedGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o MINUS { ?s <${DCAT.record}> ?o } } }")

    fun getStripped(catalogueId: String): Future<Model> =
        getStrippedGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getStrippedGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o MINUS { ?s <${DCAT.dataset}> ?o} MINUS { ?s <${DCAT.record}> ?o } } }")

    fun getGraph(name: URIRef) = tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o } }")

    fun set(catalogueId: String, model: Model) =
        setGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef, model)

    fun setGraph(name: URIRef, model: Model) = tripleStore.setGraph(name, model, false)

    fun datasetsAsFlow(catalogueId: String): Flow<Pair<DCATAPUriRef, Model>> = flow {
        emitAll(
            allDatasets(catalogueId).coAwait()
                .asFlow()
                .transform {
                    try {
                        val model = tripleStore.datasetManager.getGraph(it.graphNameRef).coAwait()

                        if (model.contains(it.resource, RDF.type, DCAT.Dataset)) {
                            emit(it to model)
                        } else {
                            log.error("Named graph for ${it.uriRef} does not contain a corresponding dcat:Dataset resource")
                        }
                    } catch (e: Exception) {
                        log.error("Fetching model", e)
                    }
                }
        )
    }

}
