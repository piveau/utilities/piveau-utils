package io.piveau.dcatap

class TripleStoreException @JvmOverloads constructor(
    val code: Int,
    message: String? = null,
    val query: String = "",
    cause: Throwable? = null
) : Exception(message ?: cause?.message, cause)
