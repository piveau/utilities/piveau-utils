package io.piveau.dcatap

import io.piveau.vocabularies.vocabulary.EDP
import io.piveau.vocabularies.vocabulary.PV
import io.vertx.core.Future
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicReference

class CatalogueManager internal constructor(private val tripleStore: TripleStore) {

    val log: Logger = LoggerFactory.getLogger(javaClass)

    fun exist(catalogueId: String) = DCATAPUriSchema.createForCatalogue(catalogueId).let {
        tripleStore.ask("ASK WHERE { GRAPH <${it.graphNameRef}> { ?s ?p ?o } }")
    }

    fun ensureExists(catalogueId: String): Future<Unit> = exist(catalogueId)
        .compose {
            if (it) Future.succeededFuture() else Future.failedFuture(TripleStoreException(404, "Catalogue Not Found"))
        }

    fun listUris(): Future<List<DCATAPUriRef>> {
        return tripleStore.select("SELECT DISTINCT ?g WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}> } }")
            .map { resultSet ->
                resultSet.asSequence().map { DCATAPUriSchema.parseUriRef(it.getResource("g").uri) }
                    .filter { it.isValid() }
                    .toList()
            }
    }

    @JvmOverloads
    fun list(offset: Int = 0, limit: Int = 100): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()
        // Take care. If more than max order by limit catalogues exists, this query will not work (usually 10.000)
        return tripleStore.select("SELECT DISTINCT ?g WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}> } } ORDER BY ?g OFFSET $offset LIMIT $limit")
            .compose { result ->
                val futures = result.asSequence()
                    .map { DCATAPUriSchema.parseUriRef(it.getResource("g").uri) }
                    .filter { it.isValid() }
                    .map { getStrippedGraph(it.uriRef) }
                    .toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map { it.list<Model>().filterNotNull() }
            .otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun subCatalogues(catalogueId: String): Future<List<DCATAPUriRef>> {
        val schema = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT ?sc WHERE { GRAPH <${schema.graphNameRef}> { <${schema.uriRef}> <${DCTerms.hasPart}> ?sc } }")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("sc").uri) }
                    .filter { it.isValid() }
                    .toList()
            }
    }

    fun allRecords(catalogueId: String): Future<List<DCATAPUriRef>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query =
            "SELECT ?r WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.record}> ?r } }"

        return tripleStore.recursiveSelect(query, 0, 5000, mutableListOf())
            .map { list ->
                list.flatMap { it.asSequence() }
                    .map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("r").uri) }
                    .filter { it.isValid() }
            }
    }

    @JvmOverloads
    fun records(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<DCATAPUriRef>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT ?r WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.record}> ?r } } OFFSET $offset LIMIT $limit")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("r").uri) }
                    .filter { it.isValid() }
                    .toList()
            }
    }

    fun allDatasets(catalogueId: String): Future<List<DCATAPUriRef>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query =
            "SELECT ?dataset WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.dataset}> ?dataset } }"

        return tripleStore.recursiveSelect(query, 0, 5000, mutableListOf())
            .map { list ->
                list.flatMap { it.asSequence() }
                    .map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("dataset").uri) }
                    .filter { it.isValid() }
            }
    }

    fun countDatasets(catalogueId: String): Future<Int> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT (count(?dataset) AS ?count) WHERE { GRAPH <${catalogueUriRef.graphName}> { <${catalogueUriRef.uriRef}> a <${DCAT.Catalog}> ; <${DCAT.dataset}> ?dataset FILTER NOT EXISTS { <${catalogueUriRef.uriRef}> <${EDP.visibility}> <${EDP.hidden}> } } }")
            .map { resultSet ->
                resultSet.asSequence()
                    .map { qs -> qs.getLiteral("count").int }
                    .toList().first()
            }
    }

    @JvmOverloads
    fun datasets(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<DCATAPUriRef>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT ?dataset WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.dataset}> ?dataset } } OFFSET $offset LIMIT $limit")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .toList()
            }
    }

    @JvmOverloads
    fun listDatasets(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT ?dataset WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.dataset}> ?dataset } } OFFSET $offset LIMIT $limit")
            .compose { result ->
                val futures = result.asSequence()
                    .map { DCATAPUriSchema.parseUriRef(it.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .map { getGraph(it.uriRef) }.toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map {
                it.list<Model>().filterNotNull()
            }.otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun allPiveauResources(catalogueId: String): Future<List<Resource>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query =
            "SELECT ?resource WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${PV.resource}> ?resource } }"

        return tripleStore.recursiveSelect(query, 0, 5000, mutableListOf())
            .map { list ->
                list.flatMap { it.asSequence() }
                    .map { qs -> qs.getResource("resource") }
            }
    }

    fun countPiveauResources(catalogueId: String): Future<Int> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT (count(?resource) AS ?count) WHERE { GRAPH <${catalogueUriRef.graphName}> { <${catalogueUriRef.uriRef}> a <${DCAT.Catalog}> ; <${PV.resource}> ?resource FILTER NOT EXISTS { <${catalogueUriRef.uriRef}> <${EDP.visibility}> <${EDP.hidden}> } } }")
            .map { resultSet ->
                resultSet.asSequence()
                    .map { qs -> qs.getLiteral("count").int }
                    .toList().first()
            }
    }

    @JvmOverloads
    fun piveauResources(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<Resource>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT ?resource WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${PV.resource}> ?resource } } OFFSET $offset LIMIT $limit")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> qs.getResource("resource") }
                    .toList()
            }
    }

    @JvmOverloads
    fun listPiveauResources(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select("SELECT ?resource WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${PV.resource}> ?resource } } OFFSET $offset LIMIT $limit")
            .compose { result ->
                val futures = result.asSequence()
                    .map { it.getResource("resource").uri }
                    .map { getGraph(it) }.toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map {
                it.list<Model>().filterNotNull()
            }.otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun addDatasetEntry(
        catalogueGraphName: String,
        catalogueUriRef: String,
        datasetUriRef: String,
        recordUriRef: String
    ): Future<Unit> {
        return tripleStore.update(
            "INSERT DATA { GRAPH <$catalogueGraphName> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <$recordUriRef> ; <http://www.w3.org/ns/dcat#dataset> <$datasetUriRef> . } }"
        )
    }


    fun addResourceEntry(
        catalogueGraphName: String,
        catalogueUriRef: String,
        resourceUriRef: String,
        recordUriRef: String
    ): Future<Unit> {
        return tripleStore.update(
            "INSERT DATA { GRAPH <$catalogueGraphName> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <$recordUriRef> ; <https://piveau.eu/ns/voc#resource> <$resourceUriRef> . } }"
        )
    }

    fun removeResourceEntry(catalogueUriRef: URIRef, resourceUriRef: URIRef,
                            recordUriRef: String): Future<Unit> {
        val catalogueDCATAPUriRef = DCATAPUriSchema.parseUriRef(catalogueUriRef)

        return tripleStore.update(
            "DELETE DATA { GRAPH <${catalogueDCATAPUriRef.graphNameRef}> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <${recordUriRef}> ; <http://www.w3.org/ns/dcat#dataset> <$resourceUriRef> . } }"
        )
    }

    fun removeDatasetEntry(catalogueUriRef: URIRef, datasetUriRef: URIRef): Future<Unit> {
        val catalogueDCATAPUriRef = DCATAPUriSchema.parseUriRef(catalogueUriRef)
        val datasetDCATAPUriRef = DCATAPUriSchema.parseUriRef(datasetUriRef)
        return tripleStore.update(
            "DELETE DATA { GRAPH <${catalogueDCATAPUriRef.graphNameRef}> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <${datasetDCATAPUriRef.recordUriRef}> ; <http://www.w3.org/ns/dcat#dataset> <$datasetUriRef> . } }"
        )
    }

    fun allDatasetIdentifiers(catalogueId: String): Future<List<String>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query =
            "SELECT DISTINCT ?identifier WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.record}> ?record } GRAPH ?d { ?record <${DCTerms.identifier}> ?identifier } }"

        return tripleStore.recursiveSelect(query, 0, 5000, mutableListOf())
            .map { list -> list.flatMap { it.asSequence() }.map { qs -> qs.getLiteral("identifier").lexicalForm } }
    }

    fun datasetIdentifiers(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<String>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        return tripleStore.select(
            """
                    SELECT DISTINCT ?identifier WHERE
                    {
                        GRAPH <${catalogueUriRef.graphNameRef}>
                        { 
                            <${catalogueUriRef.uriRef}> <${DCAT.record}> ?record 
                        } 
                        GRAPH ?d
                        {
                            ?record <${DCTerms.identifier}> ?identifier
                        }
                    } OFFSET $offset LIMIT $limit"""
        ).map { it.asSequence().map { qs -> qs.getLiteral("identifier").lexicalForm }.toList() }
    }

    fun delete(catalogueId: String) = deleteGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun deleteGraph(name: URIRef) = tripleStore.deleteGraph(name)

    fun get(catalogueId: String) = getGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getRecordsStripped(catalogueId: String): Future<Model> =
        getStrippedGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getRecordsStrippedGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o MINUS { ?s <${DCAT.record}> ?o } } }")

    fun getStripped(catalogueId: String): Future<Model> =
        getStrippedGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getStrippedGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o MINUS { ?s <${DCAT.dataset}> ?o} MINUS { ?s <${DCAT.record}> ?o } } }")

    fun getGraph(name: URIRef) = tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o } }")

    fun set(catalogueId: String, model: Model) =
        setGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef, model)

    fun setGraph(name: URIRef, model: Model) = tripleStore.setGraph(name, model, false)

    suspend fun datasetsAsFlow(catalogueId: String): Flow<Pair<DCATAPUriRef, Model>> =
        allDatasets(catalogueId).coAwait()
            .asFlow()
            .map { uriRef ->
                tripleStore.datasetManager
                    .getGraph(uriRef.graphNameRef).coAwait()
                    .let { uriRef to it }
            }
            .filter { (uriRef, model) -> model.contains(uriRef.resource, RDF.type, DCAT.Dataset) }

}
