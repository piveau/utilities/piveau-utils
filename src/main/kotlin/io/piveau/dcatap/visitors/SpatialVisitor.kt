package io.piveau.dcatap.visitors

import io.piveau.json.putIfNotBlank
import io.piveau.rdf.*
import io.piveau.vocabularies.*
import io.piveau.vocabularies.vocabulary.LOCN
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.AnonId
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.RDFVisitor
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms

object SpatialVisitor : RDFVisitor {

    override fun visitBlank(resource: Resource, id: AnonId): Any = Future.succeededFuture(JsonObject())

    override fun visitLiteral(literal: Literal): Any = Future.succeededFuture(JsonObject())

    override fun visitURI(resource: Resource, uriRef: String): Any = Future.future { promise ->
        val index = JsonObject()
        with(index) {
            if (resource.uri.contains("geonames.org")) {
                loadGeoNameFeature(resource.uri).onSuccess {
                    putIfNotBlank("label", it.getOfficialName() ?: it.getName() ?: "")
                    putIfNotBlank("id", it.getCountryCode() ?: it.getName()?.asNormalized() ?: "")
                    putIfNotBlank("resource", resource.uri)
                    promise.complete(index)
                }
            } else {
                val concept =
                    Countries.getConcept(resource) ?: Continents.getConcept(resource) ?: Places.getConcept(resource)?.let {
                        Places.countryOf(it)
                    }

                if (concept?.resource?.uri == "http://publications.europa.eu/resource/authority/continent/EUROPE") {
                    // putIfNotNull("label", concept.label("en"))
                    putIfNotBlank("label", "EU institutions")
                    putIfNotBlank("id", "eu")
                    putIfNotBlank("resource", concept.resource.uri)
                } else {
                    concept?.let {
                        putIfNotBlank("label", it.label("en"))
                        putIfNotBlank("id", Countries.iso31661alpha2(it))
                        putIfNotBlank("resource", concept.resource.uri)
                    }
                }
                promise.complete(index)
            }
        }
    }
}

object GeoSpatialVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any = JsonObject().apply {
        if (resource.isA(DCTerms.Location)) {
            parseGeoLiteralProperties(this, resource)
        }
    }

    override fun visitLiteral(literal: Literal): Any = parseGeoLiteral(literal)

    override fun visitURI(resource: Resource, uriRef: String): Any = JsonObject().apply {
        if (resource.isA(DCTerms.Location)) {
            parseGeoLiteralProperties(this, resource)
        }
    }

    private fun parseGeoLiteralProperties(index: JsonObject, resource: Resource) {
        resource.listProperties(DCAT.bbox)
            .forEachRemaining { (_, _, obj) ->
                if (obj.isLiteral) {
                    index.mergeIn(parseGeoLiteral(obj.asLiteral()), true)
                }
            }
        resource.listProperties(DCAT.centroid)
            .forEachRemaining { (_, _, obj) ->
                if (obj.isLiteral) {
                    index.mergeIn(parseGeoLiteral(obj.asLiteral()), true)
                }
            }
        resource.listProperties(LOCN.geometry)
            .forEachRemaining { (_, _, obj) ->
                if (obj.isLiteral) {
                    index.mergeIn(parseGeoLiteral(obj.asLiteral()), true)
                }
            }
    }
}
