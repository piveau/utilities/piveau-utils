package io.piveau.security;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.authentication.UsernamePasswordCredentials;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.auth.oauth2.OAuth2Options;
import io.vertx.ext.auth.oauth2.providers.KeycloakAuth;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicReference;

@DisplayName("Testing piveau auth")
@ExtendWith(VertxExtension.class)
@Disabled
public class PiveauAuthTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private PiveauAuth piveauAuth;

    private final JsonObject config = new JsonObject()
            .put("clientId", "mdo-client")
//            .put("clientSecret", "cc0ee82f-d15e-4d42-a2ef-d433fe3414bb")
            .put("clientSecret", "jMAvFvsUiboGnbvYcFLjV09I7ZD2Skvj")
            .put("tokenServerConfig", new JsonObject()
                    .put("keycloak", new JsonObject()
                            .put("serverUrl", "https://keycloak-simtest.apps.osc.fokus.fraunhofer.de")
                            .put("realm", "piveau")));

    @BeforeEach
    public void setup(Vertx vertx, VertxTestContext testContext) {
        PiveauAuth.create(vertx, new PiveauAuthConfig(config))
                .onSuccess(piveauAuth -> {
                    this.piveauAuth = piveauAuth;
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Request a client access token")
    public void requestClientToken(VertxTestContext testContext) {
        piveauAuth.requestClientToken()
                .compose(token -> {
                    System.out.println(token);
                    log.debug(token);
                    KeycloakTokenServerConfig config = (KeycloakTokenServerConfig) TokenServerConfig.create(new JsonObject()
                            .put("keycloak", new JsonObject()
                                    .put("serverUrl", "https://keycloak-simtest.apps.osc.fokus.fraunhofer.de")
                                    .put("realm", "piveau")));
//                    KeycloakResourceHelper helper = new KeycloakResourceHelper(WebClient.create(vertx), config);

                    return piveauAuth.requestUmaToken(token, "piveau-hub-repo");
//                    return WebClient.create(vertx)
//                            .getAbs("https://keycloak-simtest.apps.osc.fokus.fraunhofer.de/realms/piveau/.well-known/uma2-configuration")
//                            .bearerTokenAuthentication(token)
//                            .send();
                })
                .compose(piveauAuth::authenticate)
//                .map(HttpResponse::bodyAsJsonObject)
                .onSuccess(user -> {
                    System.out.println(user.principal().encodePrettily());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Request a user access token")
    public void requestUserToken(VertxTestContext testContext) {
        piveauAuth.requestUserToken("bob", "123456")
                .compose(piveauAuth::authenticate)
                .onSuccess(user -> {
                    System.out.println(user.principal().encodePrettily());
                    log.info(user.principal().encodePrettily());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Request a users uma token")
    public void requestUmaToken(VertxTestContext testContext) {
        piveauAuth.requestUserToken("bob", "123456")
                .compose(token -> piveauAuth.requestUmaToken(token, "piveau-hub-repo"))
                .compose(piveauAuth::authenticate)
                .onSuccess(user -> {
                    System.out.println(user.principal().encodePrettily());
                    System.out.println("User has create permission for european-union-open-data-portal: " + PiveauAuth.userHasPermission(user, "efsa", "dataset:create"));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Testing OpenID and keycloak")
    public void requestOpenID(Vertx vertx, VertxTestContext testContext) {

        AtomicReference<OAuth2Auth> oAuth2Auth = new AtomicReference<>();

        KeycloakAuth.discover(vertx, new OAuth2Options()
                        .setClientId("mdo-client")
                        .setClientSecret("jMAvFvsUiboGnbvYcFLjV09I7ZD2Skvj")
                        .setTenant("piveau")
                        .setSite("https://keycloak-simtest.apps.osc.fokus.fraunhofer.de/realms/{realm}")
                        .addSupportedGrantType("urn:ietf:params:oauth:grant-type:uma-ticket")
                )
                .compose(auth -> {
                    log.debug("OpenID Connect endpoint discovered successfully");
                    oAuth2Auth.set(auth);
                    return auth.authenticate(new UsernamePasswordCredentials("bob", "123456"));
                })
//                .compose(user -> {
//                    System.out.println("User authenticated: " + user.principal().encodePrettily());
//                    log.debug("user authenticated: {}", user.principal().encodePrettily());
//                    return oAuth2Auth.get().authenticate(new JsonObject()
//                            .put("access_token", user.principal().getString("access_token")));
//                })
                .compose(user -> {
                    System.out.println("User: " + user.principal().encodePrettily());
                    return oAuth2Auth.get().userInfo(user);
                })
                .onSuccess(jsonObject -> {
                    System.out.println(jsonObject.encodePrettily());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    /*
    Regression tests
     */

    @Test
    @DisplayName("Testing config with not existing realm")
    public void setUpWrongRealm(Vertx vertx, VertxTestContext testContext) {

        PiveauAuth.create(vertx, new PiveauAuthConfig(new JsonObject()
                        .put("clientId", "mdo-client")
                        .put("clientSecret", "jMAvFvsUiboGnbvYcFLjV09I7ZD2Skvj")
                        .put("tokenServerConfig", new JsonObject()
                                .put("keycloak", new JsonObject()
                                        .put("serverUrl", "https://keycloak-simtest.apps.osc.fokus.fraunhofer.de")
                                        .put("realm", "piveau-wrong")))))
                .onSuccess(piveauAuth -> testContext.failNow("Should not be able to create auth on wrong realm"))
                .onFailure(e -> testContext.completeNow());
    }

    @Test
    @DisplayName("Testing config with non-keycloak url")
    public void setUpWrongUrl(Vertx vertx, VertxTestContext testContext) {

        PiveauAuth.create(vertx, new PiveauAuthConfig(new JsonObject()
                        .put("clientId", "mdo-client")
                        .put("clientSecret", "jMAvFvsUiboGnbvYcFLjV09I7ZD2Skvj")
                        .put("tokenServerConfig", new JsonObject()
                                .put("keycloak", new JsonObject()
                                        .put("serverUrl", "https://piveau.apps.osc.fokus.fraunhofer.de")
                                        .put("realm", "piveau")))))
                .onSuccess(piveauAuth -> testContext.failNow("Should not be able to create auth on wrong realm"))
                .onFailure(e -> testContext.completeNow());
    }

    @Test
    @DisplayName("Testing config with wrong keycloak path")
    public void setUpWrongkeycloakPath(Vertx vertx, VertxTestContext testContext) {

        PiveauAuth.create(vertx, new PiveauAuthConfig(new JsonObject()
                        .put("clientId", "mdo-client")
                        .put("clientSecret", "jMAvFvsUiboGnbvYcFLjV09I7ZD2Skvj")
                        .put("tokenServerConfig", new JsonObject()
                                .put("keycloak", new JsonObject()
                                        .put("serverUrl", "https://keycloak-simtest.apps.osc.fokus.fraunhofer.de/auth")
                                        .put("realm", "piveau")))))
                .onSuccess(piveauAuth -> testContext.failNow("Should not be able to create auth on wrong realm"))
                .onFailure(e -> testContext.completeNow());
    }
}
