package io.piveau.dcatap;

import io.piveau.dcatap.classes.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DcatTest {

    Logger log = LoggerFactory.getLogger(DcatTest.class);

    @Test
    @DisplayName("Test the Creation of DCAT-AP Graph with a dataset")
    public void testDatasetGraph() throws ParseException {
        String datasetURI = "http://www.example.com/set/data/123";
        String distroURI = "http://www.exmaple.com/set/distro/123";
        DCATAPGraph dcatapGraph = new DCATAPGraph();

        dcatapGraph.createService("https://piveau.eu/set/service/test-service")
                .setEndPointURL("http://example.com/service/api/endpoint")
                .setEndpointDescription("http://example.com/service/api/manual")
                .setServesDataset("https://piveau.eu/set/data/test-dataset")
                .setAccessRights(":public")
                .setDescription("This is an example Data Service", "en")
                .setLicense("http://europeandataportal.eu/ontologies/od-licenses#CC-BY-SA3.0NL")
                .setTitle("Example Data Service", "en")
                .build();

        dcatapGraph.createCatalogue("https://piveau.eu/id/catalogue/test-catalog")
                .setType("dcat-ap-2")
                .setTitle("Example Catalog", "en")
                .setDescription("This is an example Catalog", "en")
                .setPublisher("http://publications.europa.eu/resource/authority/corporate-body/CCC")
                .setHomepage("http://www.catalog-homepage.com")
                .setLanguage("http://publications.europa.eu/resource/authority/language/ENG")
                .setLicense("http://europeandataportal.eu/ontologies/od-licenses#CC-BY-SA3.0NL")
                .setIssued(Instant.now())
                .setModified(Instant.now())
                .setThemeTaxonomy("http://publications.europa.eu/resource/authority/data-theme")
                .setSpatial("http://publications.europa.eu/resource/authority/country/DEU")
                .setHasPart("https://piveau.eu/id/catalogue/test-catalog")
                .setIsPartOf("https://piveau.eu/id/catalogue/test-catalog")
                .setCatalogue("https://piveau.eu/id/catalogue/test-catalog")
                .setCreator("https://piveau.eu/def/creator")
                .setRights(":public")
                .setDataset("https://piveau.eu/set/data/test-dataset")
                .build();

        Dataset dataset = dcatapGraph.createDataset(datasetURI)
                .setTitle("Example Dataset")
                .setTitle("Beispieldatensatz", "de")
                .setDescription("This is a description")
                .setPublisher("Piveau", "http://www.piveau.eu")
                .setContactPoint("Piveau Team", "info@piveau.eu")
                .addKeyword("test")
                .addKeyword("piveau")
                .setConformsTo("Conforms To")
                .setAccessRights(":public")
                .setType("https://piveau.eu/def/type/dataset")
                .setLanguage("http://publications.europa.eu/resource/authority/language/ENG")
                .setTheme("http://publications.europa.eu/resource/authority/data-theme/ENVI")
                .setCreator("https://piveau.eu/def/creator")
                .setLandingPage("http://landingpage.de")
                .setSpatial("https://piveau.eu/def/example-location")
                .setSpatialResolutionInMeters("10")
                .setAccrualPeriodicity("http://publications.europa.eu/resource/authority/frequency/ANNUAL")
                .setPage("http://www.documentation.com")
                .setDCATIdentifier("123456789")
                .setHasVersion("https://piveau.eu/set/data/test-dataset-2")
                .setIsVersionOf("https://piveau.eu/set/data/test-dataset-3")
                .setIsReferencedBy("http://example/reference-description")
                .setSource("https://piveau.eu/set/data/test-dataset-4")
                //.setADMSIdentifier("http://www.doi.org/123456789", "DPO Identifier")
                .setRelation("http://relation.com")
                .setSample("https://piveau.eu/set/distribution/1")
                .setQualifiedRelation("https://piveau.eu/def/relationship")
                .setQualifiedAttribution("Responsible Agent", "mailto:agent@example.org", "http://agent.example.com")
                .setVersionInfo("1.0.0")
                .setVersionNote("Release", "en")
                .setProvenance("This is a provenance statement", "en")
                .setTemporal(Instant.now(), Instant.now())
                .setTemporalResolution("P20M")
                .setIssued(Instant.now())
                .setModified(Instant.now())
                .setWasGeneratedBy(Instant.now(), Instant.now())
                .setADMSIdentifier("http://www.doi.org/123456789", "http://purl.org/spar/datacite/doi", "DPO Identifier" );

        dataset.createDistribution("http://www.exmaple.com/set/distro/123")
                .setAccessURL("http://download.com")
                .setDownloadURL("http://download.de")
                .setFormat("http://publications.europa.eu/resource/authority/file-type/XLS")
                .setTitle("Distribution 1")
                .setDescription("This is a simple distribution")
                .setLicense("https://creativecommons.org/licenses/by/4.0/")
                .setAccessService("https://piveau.eu/set/service/test-service")
                .setIssued(Instant.now())
                .setModified(Instant.now())
                .setCompressFormat("http://www.iana.org/assignments/media-types/application/gzip")
                .setPackageFormat("http://www.iana.org/assignments/media-types/application/gzip")
                .setLanguage("http://publications.europa.eu/resource/authority/language/ENG")
                .setSpatialResolutionInMeters("10")
                .setTemporalResolution("P20M")
                .setHasPolicy("publish")
                .setRights(":public")
                .setConformsTo("Distribution Standard")
                .setPage("http://www.documentation2.com")
                .setStatus("http://purl.org/adms/status/Completed")
                .setByteSize("25255")
                .setCheckSum("4c3a106cf3925d14d986d2b0839f5d1e")
                .setAvailability("stable")
                .build();

        dataset.createDistribution()
                .setAccessURL("http://download2.com")
                .setFormat("CSV")
                .setDownloadURL("http://downloadurl.com")
                .setMediaType("http://www.iana.org/assignments/media-types/text/csv")
                .build();

        // Build here
        dataset.build();
        String result = dcatapGraph.build().asTurtle();

        System.out.println(result);

        Model model = ModelFactory.createDefaultModel();
        RDFParser.fromString(result).lang(Lang.TURTLE).build().parse(model);

        Resource resource = model.getResource(datasetURI);
        assertEquals(resource.getURI(), datasetURI);
        assertEquals(resource.getProperty(DCTerms.description).getLiteral().getString(), "This is a description");
        assertEquals(resource.listProperties(DCAT.distribution).toList().size(), 2);

        resource = model.getResource(distroURI);
        assertTrue(resource.getProperty(DCTerms.format).getObject().isResource());
    }

    @Test
    @DisplayName("Test the Creation of DCAT-AP Graph with a distribution")
    public void testDistributionGraph() {
        String distroURI = "http://www.exmaple.com/set/distro/123";
        DCATAPGraph dcatapGraph = new DCATAPGraph();

        dcatapGraph.createDistribution("http://www.exmaple.com/set/distro/123")
                .setAccessURL("http://download.com")
                .setFormat("CSV")
                .setTitle("Distribution 1")
                .setIdentifier("my-own-identifier")
                .build();

        String result = dcatapGraph.build().asTurtle();

        Model model = ModelFactory.createDefaultModel();
        RDFParser.fromString(result).lang(Lang.TURTLE).build().parse(model);
        Resource resource = model.getResource(distroURI);
        assertEquals(resource.getProperty(DCTerms.identifier).getLiteral().getString(), "my-own-identifier");
        assertTrue(resource.getProperty(DCTerms.format).getObject().isLiteral());
        assertTrue(resource.getProperty(DCAT.accessURL).getObject().isResource());
        //log.info(result);
    }

}
