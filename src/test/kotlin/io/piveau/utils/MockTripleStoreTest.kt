package io.piveau.utils

import io.piveau.rdf.toModel
import io.piveau.test.MockTripleStore
import io.piveau.test.MockTripleStore.Companion.getDefaultConfig
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.junit5.Timeout
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.http.HttpStatus
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.SKOS
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

@DisplayName("Testing mock triple store")
@ExtendWith(VertxExtension::class)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Timeout(2, timeUnit = TimeUnit.MINUTES)
class MockTripleStoreTest {

    private val LOGGER: Logger = LoggerFactory.getLogger(MockTripleStoreTest::class.java)
    val testGraphName: String = "https://piveau.io/graph/test1"

    val testGraph = """
        @prefix dc:         <http://purl.org/dc/terms/> .
        @prefix skos:       <http://www.w3.org/2004/02/skos/core#> .
        @prefix piveau:     <http://piveau.io/ns/test#> .

        piveau:test
            a                   skos:Concept ;
            skos:prefLabel      "Test"@en ;
            dc:description      "Just simple test."@en ;
            dc:identifier       "TEST" .
    """.trimIndent()

    private val config: JsonObject = getDefaultConfig()

    @BeforeAll
    fun setup(vertx: Vertx, testContext: VertxTestContext) {
        MockTripleStore().deploy(vertx).onComplete(testContext.succeedingThenComplete())
    }

    @Test
    @Order(1)
    fun `Put a graph`(vertx: Vertx, testContext: VertxTestContext) {

        WebClient.create(vertx)
            .putAbs("${config.getString("address")}${config.getString("graphEndpoint")}")
            .addQueryParam("graph", testGraphName)
            .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "text/turtle")
            .sendBuffer(Buffer.buffer(testGraph))
            .onSuccess {
                assert(HttpStatus.SC_CREATED == it.statusCode())
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)

    }


    @Test
    @Order(2)
    fun `Get a graph`(vertx: Vertx, testContext: VertxTestContext) {

        WebClient.create(vertx)
            .getAbs("${config.getString("address")}${config.getString("graphEndpoint")}")
            .addQueryParam("graph", testGraphName)
            .putHeader(HttpHeaders.ACCEPT.toString(), "text/turtle")
            .send()
            .onSuccess {
                assert(HttpStatus.SC_OK == it.statusCode())
                assert(
                    it.bodyAsString().toByteArray().toModel("text/turtle").isIsomorphicWith(
                        testGraph.toByteArray().toModel(
                            "text/turtle"
                        )
                    )
                )
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)

    }

    @Test
    @Order(3)
    fun `Query a Select`(vertx: Vertx, testContext: VertxTestContext) {

        WebClient.create(vertx)
            .getAbs("${config.getString("address")}${config.getString("queryEndpoint")}")
            .addQueryParam("query", "SELECT ?test WHERE { graph <$testGraphName> { ?test a <${SKOS.Concept}> } }")
            .send()
            .onSuccess {
                assert(HttpStatus.SC_OK == it.statusCode())
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)

    }

    @Test
    @Order(4)
    fun `Update Graph`(vertx: Vertx, testContext: VertxTestContext) {
        WebClient.create(vertx)
            .postAbs("${config.getString("address")}${config.getString("queryAuthEndpoint")}")
            .sendBuffer(Buffer.buffer("INSERT DATA { GRAPH <$testGraphName> { <http://piveau.io/ns/test#test> <${DCTerms.title}> \"Testing TripleStore\"@en . } }"))
            .onSuccess {
                testContext.verify {
                    Assertions.assertEquals(HttpStatus.SC_OK, it.statusCode())
                }
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    @Test
    @Order(5)
    fun `Get a graph again`(vertx: Vertx, testContext: VertxTestContext) {

        WebClient.create(vertx)
            .getAbs("${config.getString("address")}${config.getString("graphEndpoint")}")
            .addQueryParam("graph", testGraphName)
            .putHeader(HttpHeaders.ACCEPT.toString(), "text/turtle")
            .send()
            .onSuccess { response ->
                assert(HttpStatus.SC_OK == response.statusCode())
                LOGGER.info(response.bodyAsString())
                val testmodel = response.bodyAsString().toByteArray().toModel("text/turtle")

                val pResource = testmodel.getResource("http://piveau.io/ns/test#test")

                testContext.verify {
                    assert(
                        testmodel.contains(pResource, DCTerms.title, "Testing TripleStore", "en")

                    )
                    assert(
                        testmodel.contains(pResource, DCTerms.description, "Just simple test.", "en")

                    )
                    assert(
                        testmodel.contains(pResource, DCTerms.identifier, "TEST")

                    )
                }
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)

    }

    @Test
    @Order(6)
    fun `Delete Graph`(vertx: Vertx, testContext: VertxTestContext) {

        WebClient.create(vertx)
            .deleteAbs("${config.getString("address")}${config.getString("graphEndpoint")}")
            .addQueryParam("graph", testGraphName)
            .send()
            .onSuccess {
                assert(HttpStatus.SC_OK == it.statusCode())
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

}