package io.piveau.utils

import io.piveau.dcatap.TripleStore
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@DisplayName("Testing dataset manager")
@ExtendWith(VertxExtension::class)
class DatasetManagerTest {

    @Test
    @Disabled
    fun `Identify a dataset`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx)
        tripleStore.datasetManager.identify("", "").onSuccess {
            testContext.completeNow()
        }.onFailure { testContext.failNow(it) }
    }

}
