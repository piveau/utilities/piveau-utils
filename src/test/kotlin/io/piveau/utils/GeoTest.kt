package io.piveau.utils

import io.piveau.rdf.gmlDatatype
import io.piveau.rdf.parseGeoLiteral
import org.apache.jena.rdf.model.ModelFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.locationtech.jts.io.WKTReader
import org.locationtech.jts.io.geojson.GeoJsonWriter

class GeoTest {

    @Test
    fun `Testing wkt formats`() {
        Assertions.assertDoesNotThrow {
            val geometry =
                WKTReader().read("POLYGON((-11.074 35.746,28.652 35.746,28.652 59.623,-11.074 59.623,-11.074 35.746))")
            val writer = GeoJsonWriter().apply { setEncodeCRS(false) }
            println(writer.write(geometry))
        }
    }

    @Test
    fun `Testing gml format`() {
        val literal = ModelFactory.createDefaultModel().createTypedLiteral("""<gml:Envelope srsName="http://www.opengis.net/def/crs/OGC/1.3/CRS84"><gml:lowerCorner>7,2396 60,2627</gml:lowerCorner><gml:upperCorner>32,9351 71,4018</gml:upperCorner></gml:Envelope>""", gmlDatatype)
        val json = parseGeoLiteral(literal)
        println(json.encodePrettily())
    }

}