package io.piveau.utils

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Testing date time utils")
internal class DateTimeTest {

    @Test
    fun  `Parsing several date and dateTime strings`() {
        assertNotNull(normalizeDateTime("2019-07-01T12:00:20Z"))
        assertNotNull(normalizeDateTime("2019-07-01T12:00:20.00Z"))
        assertNotNull(normalizeDateTime("2019-07-01T12:00:20.00+01:00"))
        assertNotNull(normalizeDateTime("2019-07-01T12:00:20.00+02"))
        assertNotNull(normalizeDateTime("2019-07-01T12:00:20"))
        assertNotNull(normalizeDateTime("2019-07-01T12:00:20.000000"))
        assertNotNull(normalizeDateTime("2022-06-27T11:59:23.182476"))
        assertNotNull(normalizeDateTime("2019-07-01"))
        assertNotNull(normalizeDateTime("2019/06/01"))
        assertNull(normalizeDateTime("blabla"))
    }

}