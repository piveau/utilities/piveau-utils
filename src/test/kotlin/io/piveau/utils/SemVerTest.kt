package io.piveau.utils

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.Test
import kotlin.IllegalArgumentException

@DisplayName("Testing SemVer class")
@ExperimentalUnsignedTypes
class SemVerTest {

    @Test
    fun `Check validity`() {
        assertFalse("blabla".isSemVer())
        assertFalse("2.0".isSemVer())
        assertFalse("2.0.0.1".isSemVer())
        assertTrue("2.0.0".isSemVer())
        assertTrue("1.0.0-a+1".isSemVer())
        assertTrue("1.0.0+a".isSemVer())
    }

    @Test
    fun `Check constructor`() {
        assertThrows<IllegalArgumentException> { SemVer("blabla") }
        assertThrows<IllegalArgumentException> { SemVer("2.0") }
        assertThrows<IllegalArgumentException> { SemVer("2.0.0.1") }
        assertDoesNotThrow { SemVer("3.0.3") }
        assertDoesNotThrow { SemVer() }
    }

    @Test
    fun `Check decomposition`() {
        with(SemVer("3.0.3")) {
            assertEquals(3U, major)
            assertEquals(0U, minor)
            assertEquals(3U, patch)
            assertTrue(preRelease.isBlank())
            assertTrue(buildMetadata.isBlank())
        }
        with(SemVer("2.0.0-a.b.c+444")) {
            assertEquals(2U, major)
            assertEquals(0U, minor)
            assertEquals(0U, patch)
            assertEquals("a.b.c", preRelease)
            assertEquals("444", buildMetadata)
        }
    }

    @Test
    fun `Check equality operator`() {
        assertTrue(SemVer("1.0.0") == SemVer("1.0.0"))
        assertTrue(SemVer("1.0.0-alpha") == SemVer("1.0.0-alpha+tmp"))
        assertTrue(SemVer("1.0.0") == SemVer("1.0.0+tmp"))
    }

    @Test
    fun `Check comparator operator`() {
        assertTrue(SemVer("1.0.0") < SemVer("1.0.1"))
        assertTrue(SemVer("1.0.0") >= SemVer("0.0.1"))
        assertTrue(SemVer("1.0.0") < SemVer("2.0.0"))
        assertTrue(SemVer("3.0.0") < SemVer("3.1.0"))
        assertTrue(SemVer("3.0.0") <= SemVer("3.0.0"))
        assertTrue(SemVer("3.0.0") >= SemVer("3.0.0"))
        assertTrue(SemVer("3.1.0") < SemVer("3.1.1"))
        assertTrue(SemVer("3.0.1") > SemVer("3.0.0"))
        assertTrue(SemVer("1.0.0-prerelease") < SemVer("1.0.0"))
        assertTrue(SemVer("1.0.0-prerelease2") >= SemVer("1.0.0-prerelease1"))
        assertTrue(SemVer("1.0.0-prerelease.a") < SemVer("1.0.0-prerelease.b"))
        assertTrue(SemVer("1.0.0-prerelease.a") <= SemVer("1.0.0-prerelease.a"))

        // Mentioned cases in SemVer spec
        assertTrue(SemVer("1.0.0-alpha") < SemVer("1.0.0-alpha.1"))
        assertTrue(SemVer("1.0.0-alpha.1") < SemVer("1.0.0-alpha.beta"))
        assertTrue(SemVer("1.0.0-alpha.beta") < SemVer("1.0.0-beta"))
        assertTrue(SemVer("1.0.0-beta") < SemVer("1.0.0-beta.2"))
        assertTrue(SemVer("1.0.0-beta.2") < SemVer("1.0.0-beta.11"))
        assertTrue(SemVer("1.0.0-beta.11") < SemVer("1.0.0-rc.1"))
        assertTrue(SemVer("1.0.0-rc.1") < SemVer("1.0.0"))
    }

    @Test
    fun `Check to string`() {
        assertEquals("0.0.0", SemVer().toString())
        assertEquals("1.0.0-alpha.1+2312", SemVer("1.0.0-alpha.1+2312").toString())
    }

    @Test
    fun `Modify parts`() {
        assertEquals(SemVer("1.0.0-beta.0+2312"), SemVer("1.0.0-alpha.1+2312").apply {
            preRelease = "beta.0"
        })
        assertThrows<IllegalArgumentException> {
            SemVer().preRelease = "-00.alpha#"
        }
    }

}
