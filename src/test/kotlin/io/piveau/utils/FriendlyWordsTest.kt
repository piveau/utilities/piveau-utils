package io.piveau.utils

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Testing friendly words generation")
class FriendlyWordsTest {

    @Test
    fun `Generate out of two categories`() {
        println(FriendlyWords().generateOutOfTwo())
    }

    @Test
    fun `Generate out of three categories`() {
        println(FriendlyWords().generateOutOfThree())
    }

    @Test
    fun `Generate out of four categories`() {
        println(FriendlyWords().generateOutOfFour())
    }

}