package io.piveau

import io.piveau.dqv.addMeasurement
import io.piveau.dqv.removeMeasurement
import io.piveau.dqv.replaceMeasurement
import io.piveau.rdf.asString
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MetricsTest {

    private val log: Logger = LoggerFactory.getLogger(javaClass)

    @Test
    fun `Test adding a measurement to model`() {
        val model = ModelFactory.createDefaultModel()
        model.addMeasurement(ModelFactory.createDefaultModel().createResource("urn:test1"), PV.accessRightsAvailability, true)
        log.info(model.asString(Lang.TURTLE))
    }

    @Test
    fun `Test removing a measurement from model`() {
        val model = ModelFactory.createDefaultModel()
        model.addMeasurement(ModelFactory.createDefaultModel().createResource("urn:test1"), PV.accessRightsAvailability, true)
        model.addMeasurement(ModelFactory.createDefaultModel().createResource("urn:test1"), PV.formatMatch, false)
        model.removeMeasurement(ModelFactory.createDefaultModel().createResource("urn:test1"), PV.accessRightsAvailability)
        log.info(model.asString(Lang.TURTLE))
    }

    @Test
    fun `Test replacing a measurement in model`() {
        val model = ModelFactory.createDefaultModel()
        model.addMeasurement(ModelFactory.createDefaultModel().createResource("urn:test1"), PV.accessRightsAvailability, true)
        model.replaceMeasurement(ModelFactory.createDefaultModel().createResource("urn:test1"), PV.accessRightsAvailability, false)
        log.info(model.asString(Lang.TURTLE))
    }

}
