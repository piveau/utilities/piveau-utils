package io.piveau.rdf

import org.apache.jena.riot.Lang
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.slf4j.LoggerFactory

//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PreProcessingTest {

    private val log = LoggerFactory.getLogger(javaClass)

    @Test
    fun `Test rdf pre-processing and fixing malformed uris in n-triples`() {
        val rdf = """
            <urn:one \t> <urn:two/bla> <urn:three\tfour%> .
        """.trimIndent().toByteArray()

        val (content, mimeType) = preProcess(
            rdf,
            "application/n-triples",
            "http://example.com"
        )
        val model = content.toByteArray().toModel(mimeType.asRdfLang())
        log.debug(model.presentAs(Lang.TURTLE))
    }

    @Test
    fun `Test rdf pre-processing and fixing malformed uris in json-ld`() {
        // language=JSON
        val rdf = """
            {
                "@id": "urn:one \t",
                "urn:two/bla": {
                    "@id": "urn: three\tfour%"
                },
                "urn:five": {
                    "@id": "/relative with spaces"
                }
            }
        """.trimIndent().toByteArray()

        val (content, mimeType) = preProcess(
            rdf,
            "application/ld+json"
        )
        val model = content.toByteArray().toModel(mimeType.asRdfLang())
        log.debug(model.presentAs(Lang.TURTLE))
    }

    @Test
    fun `Test rdf pre-processing and fixing malformed uris in rdf+xml`() {
        // language=XML
        val rdf = """
            <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:test="urn:two/">
                <rdf:Description rdf:about="urn:one">
                    <test:bla>
                        <rdf:Description rdf:about="urn: three\tfour%" />
                    </test:bla>
                    <test:five>
                        <rdf:Description rdf:about="/relative with spaces" />
                    </test:five>
                </rdf:Description>
            </rdf:RDF>
        """.trimIndent().toByteArray()

        val (content, mimeType) = preProcess(
            rdf,
            "application/rdf+xml",
            "http://example.com"
        )
        val model = content.toByteArray().toModel(mimeType.asRdfLang())
        log.debug(model.presentAs(Lang.TURTLE))
    }

    @Test
    fun `Test rdf pre-processing and fixing malformed uris in turtle`() {
        val rdf = """
            <urn:one \t> <urn:two/bla> <urn: 
            three\tfour%> .
            <urn:test> <dcat:accessURL> </downloads/Interim Case …form_EST_31JAN2020.docx> .
        """.trimIndent().toByteArray()

        val (content, mimeType) = preProcess(rdf, "text/turtle", "http://example.com")
        val model = content.toByteArray().toModel(mimeType.asRdfLang())
        log.debug(model.presentAs(Lang.TURTLE))
    }

}
