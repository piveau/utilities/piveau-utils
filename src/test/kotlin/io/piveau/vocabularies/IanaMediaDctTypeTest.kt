package io.piveau.vocabularies

import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.assertFalse

@DisplayName("Testing Iana Media type validator")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IanaMediaTypeTest {

    val model = ModelFactory.createDefaultModel().apply {
        readTurtleResource("DistributionsWithMediaType.ttl")
    }

    @BeforeAll
    fun setup(vertx: Vertx, testContext: VertxTestContext) {
        testContext.completeNow()
    }

    @Test
    fun `load common valid Media Type in application registry`(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/commonValidMediaType")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

    @Test
    fun `load common valid Media Type in application registry with https`(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/commonValidMediaTypeAndHTTPS")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }
    @Test
    fun `load common valid Media Type in application registry with #resource`(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/commonValidMediaTypeAnd#Resource")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }
    @Test
    fun `load common valid Media Type in application registry with https and resource`(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/commonValidMediaTypeWithHtppsAnd#Resource")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

    @Test
    fun `load common w3 example`(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/w3example")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

    @Test
    fun `load uncommon valid Media Type in application registry `(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/uncommonValidMediaType")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }


    @Test
    fun `load invalid Media Type in text registry `(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/invalidMediaType")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertFalse { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

    @Test
    fun `loadMedia Type in  invalid  application registry `(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/invalidRegistry")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertFalse { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

    @Test
    fun `load Format `(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType =
            model.getResource("https://europeandataportal.eu/set/distribution/format")
                .getPropertyResourceValue(DCTerms.format)

        testContext.verify {
            assertFalse { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

    @Test
    fun `load valid Media Type that is stored as text `(vertx: Vertx, testContext: VertxTestContext) {
        val mediatype = model.getResource("https://europeandataportal.eu/set/distribution/validTextMediaType")
            .getProperty(DCAT.mediaType).string

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediatype) }
        }

        testContext.completeNow()
    }

    @Test
    fun `load invalid Media Type that is stored as text `(vertx: Vertx, testContext: VertxTestContext) {
        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/invalidTextMediaType")
            .getProperty(DCAT.mediaType).string

        testContext.verify {
            assertFalse { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

    @Test
    fun `load valid Media Type in application registry with remote`(vertx: Vertx, testContext: VertxTestContext) {
        initRemotes(vertx, prefetch = false)

        val mediaType = model.getResource("https://europeandataportal.eu/set/distribution/commonValidMediaType")
            .getPropertyResourceValue(DCAT.mediaType)

        testContext.verify {
            assertTrue { IanaType.isMediaType(mediaType) }
        }

        testContext.completeNow()
    }

}