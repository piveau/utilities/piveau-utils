package io.piveau.vocabularies

import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.assertTrue

@DisplayName("Testing Geonames resolver")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GeonamesTest {

    @Test
    fun `Test geonames class`(vertx: Vertx, testContext: VertxTestContext) {
        initRemotes(vertx, prefetch = false)

        loadGeoNameFeature("https://sws.geonames.org/2921044/")
            .onSuccess {
                testContext.verify {
                    assertEquals("Germany", it.getOfficialName())
                    assertEquals("DE", it.getCountryCode())
                }

                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

}