# Module piveau-utils

Utility classes and functions for the piveau context.
 
# Package io.piveau.dcatap

Classes to build a DCAT-AP dataset graph, including distributions.

Usage:

```java
DCATAPGraph DCATAPGraph = new DCATAPGraph();

Distribution distribution = new Distribution("http://www.exmaple.com/set/distro/123");
distribution
    .addAccessURL("http://download.com")
    .addFormat("http://publications.europa.eu/resource/authority/file-type/XLS")
    .addTitle("Distribution 1")
    .addDescription("This is a simple distribution")
    .addLicense("https://creativecommons.org/licenses/by/4.0/");

Dataset dataset = new Dataset(datasetURI);
dataset
    .addTitle("Example Dataset")
    .addTitle("Beispieldatensatz", "de")
    .addDescription("This is a description")
    .addPublisher("Piveau", "http://www.piveau.eu")
    .addContactPoint("Piveau Team", "info@piveau.eu")
    .addKeyword("test")
    .addKeyword("piveau")
    .addIssued(Instant.parse("2015-04-02T13:00:00+02:00"))
    .addModified(Instant.now())
    .addDistribution(distribution)

String result = DCATAPGraph.asTurtle();
```

# Package io.piveau.dqv

Classes and functions around DQV metrics.

# Package io.piveau.json

Useful extensions for Vert.x `JsonObject` and `JsonArray`.

# Package io.piveau.log

Logging for piveau environments.

# Package io.piveau.rdf

Helper and extensions around RDF and apache Jena.

# Package io.piveau.utils

Miscellaneous classes, methods and functions.

# Package io.piveau.test

Classes and functions for testing purposes.

# Package io.piveau.vocabularies

Classes for many RDF vocabularies, especially relevant for DCAT-AP.

# Package io.piveau.vocabularies.vocabulary

Shortcut definitions for several vocabularies. 
